<?php
class SSTech_CancelOrder_OrderController
    extends Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();

        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    /*
        @params for cancel the order along with email

    */
    public function cancelAction()
    {
        $order = Mage::getModel('sales/order')->load(
            $this->getRequest()->getParam('order_id')
        );

        if ($order->getId()) {
           // if (Mage::helper('cancelorder/customer')->canCancel($order)) {
                try {
                    $order->cancel();

                    if ($status = Mage::helper('cancelorder/customer')->getCancelStatus($store)) {
                        $order->addStatusHistoryComment('', $status)
                              ->setIsCustomerNotified(1);
                    }

                    $order->save();
                     // If sending transactionnal email is enabled in system configuration, we send the email
                    //if(Mage::getStoreConfigFlag('sales/cancel/send_email')) {
                    $order->sendOrderUpdateEmail();
                    //}
                    
                    
                    //my custom code to update campaign ordered quantity and notify admin
                    $item = $order -> getAllVisibleItems();
                    
                    foreach( $item as $val ){
                        $product_id = $val->getProductId();
                        // $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($val->getProductId());
                        // echo $val->getProductId();exit;
                        // if (isset($parentIds[0])) {
                             
                            $product = Mage::getModel('catalog/product')->load( $val->getProductId() );
                            $quantity_ordered = $product -> getQuantityOrdered();
                            $ordered_item_qty = $val     -> getQtyOrdered();
                            
                            $updated_quantity = $quantity_ordered - $ordered_item_qty;
                           // $product -> setData('campaign_status', $updated_quantity);
                            //$product -> save();
                            
                            $data['quantity_ordered'] = $updated_quantity;
                            $model = Mage::getModel('catalog/product')->load($val->getProductId())->addData($data);
                            $model->setId($val->getProductId())->save();
                            
                            
                            //get incremental order id
                            $Incrementid = $order->getIncrementId();
                            
                            $connectionRead = Mage::getSingleton('core/resource')->getConnection('core_read');
                            $sql = "SELECT id FROM paypaladaptive_transaction WHERE order_id = '".$Incrementid."'";
                            $rowArray = $connectionRead->fetchRow($sql); 
                           // echo $rowArray['id'];exit;
                            
                            $connection  = Mage::getSingleton('core/resource')->getConnection('core_write'); 
                            $condition   = array($connection->quoteInto('id=?', $rowArray['id']));
                            $connection->delete('paypaladaptive_transaction', $condition);
                            
                            
                        // }
                         
                    }
                    //custom code end
                    
                    //mail sent to the customer already create order for this product id any
                                    $sql = "SELECT so.customer_email, so.customer_firstname, so.customer_lastname, si.product_id FROM sales_flat_order_item as si INNER JOIN sales_flat_order as so ON (si.order_id = so.entity_id && so.status = 'processing' && so.increment_id != '".$Incrementid."') WHERE si.product_id = '".$product_id."' GROUP BY so.customer_email";
                                    $ord = $connectionRead->fetchAll($sql);

                                    if(count($ord) > 0){

                                      foreach( $ord as $val ){

                                            $emailTemplate = Mage::getModel('core/email_template')->loadDefault('order_cancel_notification_customer_already_ordered');
                                            $customer_name = $val['customer_firstname'].' '. $val['customer_lastname'];
                                            $customer_mail = $val['customer_email'];


                                            $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); 

                                            $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); 


                                            $emailTemplate->setSenderName($storename);  
                                            $emailTemplate->setSenderEmail($storeemail);

                                            $pro = Mage::getModel('catalog/product')->load($product_id);
                                            $templateParams=array(
                                                 'name'             => $customer_name,
                                                 'campaign_name'    => $pro -> getName(),
                                                 'sku'              => $pro -> getSku(),
                                                 'sales_target'     => $pro -> getSalesTarget(),
                                                 'quantity_ordered' => $pro -> getQuantityOrdered()
                                            );

                                            $emailTemplate->send($customer_mail, $customer_name, $templateParams);
                                      }

                                    }
                                    //mail sent code end
                    
                    
                    Mage::getSingleton('catalog/session')
                        ->addSuccess($this->__('Your order has been canceled.'));
                } catch (Exception $e) {
                    Mage::getSingleton('catalog/session')
                        ->addException($e, $this->__('Cannot cancel your order.'));
                }
           /* } else {
                Mage::getSingleton('catalog/session')
                    ->addError($this->__('Cannot cancel your order.'));
            }*/

            $this->_redirect('sales/order/history');

            return;
        }

        $this->_forward('noRoute');
    }
}
