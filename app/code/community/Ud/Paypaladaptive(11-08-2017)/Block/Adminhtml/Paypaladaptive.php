<?php


class Ud_Paypaladaptive_Block_Adminhtml_Paypaladaptive extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_paypaladaptive";
	$this->_blockGroup = "paypaladaptive";
	$this->_headerText = Mage::helper("paypaladaptive")->__("Paypaladaptive Manager");
	$this->_addButtonLabel = Mage::helper("paypaladaptive")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	}

}