<?php
/*
Mygateway Payment Controller
By: Junaid Bhura
www.junaidbhura.com
*/

class Ud_Paypaladaptive_PaymentController extends Mage_Core_Controller_Front_Action {
    
    private $PROXY_HOST;
    private $PROXY_PORT;

    private $Env;

    private $API_UserName;
    private $API_Password;
    private $API_Signature;

    private $API_AppID;

    private $API_Endpoint;

    private $USE_PROXY;


	// The redirect action is triggered when someone places an order
	public function redirectAction() {
		
		$this->loadLayout();
       /* $block = $this->getLayout()->createBlock('Mage_Core_Block_Template','paypaladaptive',array('template' => 'paypaladaptive/redirect.phtml'));
		$this->getLayout()->getBlock('content')->append($block);*/
                $this->renderLayout();
	}
	
	// The response action is triggered when your gateway sends back a response after processing the customer's payment
	public function responseAction() {
           
                        $this->PROXY_HOST='127.0.0.1';
			$this->PROXY_PORT='808';
			$en=Mage::getStoreConfig('payment/paypaladaptive/paypal_env');
			if($en=='0'){
			$this->Env='sandbox';
			}else{
			$this->Env='live';
			}
			
			$this->API_UserName=Mage::getStoreConfig('payment/paypaladaptive/apiuser');
			$this->API_Password=Mage::getStoreConfig('payment/paypaladaptive/apipassword');
			$this->API_Signature=Mage::getStoreConfig('payment/paypaladaptive/apisignature');
			$this->API_AppID=Mage::getStoreConfig('payment/paypaladaptive/api_appid');
			
			 if ($this->Env == "sandbox")
				  $this->API_Endpoint='https://svcs.sandbox.paypal.com/AdaptivePayments/PreapprovalDetails';
			 else
				  $this->API_Endpoint='https://svcs.paypal.com/AdaptivePayments/PreapprovalDetails';
			
			$this->USE_PROXY=false;
                        
                        //$res=$this->CallPaymentDetails();
		
		//echo 'mail sent';exit;
		$order_id=$this->getRequest()->getParam("order_id");
                
                
                
                
		if($order_id!='') {
                    
                $paypalModel = Mage::getModel('paypaladaptive/paypaladaptive')->getCollection();
                $paypalModel->addFieldToFilter('order_id',$order_id);
                $data=$paypalModel->getData();
                $data=$data[0];
                
                $res=$this->CallPreapprovalDetails($data['preapprovalKey']);
               
		mail('unified.ujjal@gmail.com',"magento paypal response","<pre>".print_r($_POST,true)."</pre>");	
			/*
			/* Your gateway's code to make sure the reponse you
			/* just got is from the gatway and not from some weirdo.
			/* This generally has some checksum or other checks,
			/* and is provided by the gateway.
			/* For now, we assume that the gateway's response is valid
			*/
			
			//$validated = true;
			$orderId = substr($order_id, 0, -1); // Generally sent by gateway
                        
                       // print_r($res);exit;
			
			//if($res['paymentInfoList.paymentInfo(0).transactionStatus']=='COMPLETED') {
                        if($res['responseEnvelope.ack']=='Success' && $res['approved'] == 'true' && $res['status'] == 'ACTIVE') {
                            
                            $paypalModel = Mage::getModel('paypaladaptive/paypaladaptive')->load($data['id']);
                           
                            $paypalModel->setTransactionId($res['paymentInfoList.paymentInfo(0).transactionId']);
                            $paypalModel->setStatus("complete");
                             $paypalModel->save();
				// Payment was successful, so update the order's state, send order email and move to the success page
				$order = Mage::getModel('sales/order');
				$order->loadByIncrementId($orderId);
                                //echo $orderId;exit;
				$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
				
				$order->sendNewOrderEmail();
				$order->setEmailSent(true);
				
				$order->save();
                                
//                                try {
//                                if(!$order->canInvoice())
//                                {
//                                Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
//                                }
//                                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
//                                if (!$invoice->getTotalQty()) {
//                                Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
//                                }
//                                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
//                                $invoice->register();
//                                $transactionSave = Mage::getModel('core/resource_transaction')
//                                ->addObject($invoice)
//                                ->addObject($invoice->getOrder());
//                                $transactionSave->save();
//                                }
//                                catch (Mage_Core_Exception $e) {
//                                }

			
				Mage::getSingleton('checkout/session')->unsQuoteId();
				
				Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=>true));
			}
			else {
				// There is a problem in the response we got
				$this->cancelAction();
				Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/failure', array('_secure'=>true));
			}
		}
		else
			Mage_Core_Controller_Varien_Action::_redirect('');
		
	}
	
	// The cancel action is triggered when an order is to be cancelled
	public function cancelAction() {
        
            if (Mage::getSingleton('checkout/session')->getLastRealOrderId()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
            if($order->getId()) {
				// Flag the order as 'cancelled' and save it
				$order->cancel()->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, 'Gateway has declined the payment.')->save();
			}
        }
        
        Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/failure', array('_secure'=>true));
	}
        
        
        
        
        
        
        
        
        
        
        
        
        

private function generateCharacter () {
  $possible = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
  return $char;
 }

 private function generateTrackingID () {
 $GUID = $this->generateCharacter().$this->generateCharacter().$this->generateCharacter();
 $GUID .=$this->generateCharacter().$this->generateCharacter().$this->generateCharacter();
 $GUID .=$this->generateCharacter().$this->generateCharacter().$this->generateCharacter();
 return $GUID;
 }

 /*
 '-------------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the Refund API Call.
 '   The API credentials used in a Pay call can make the Refund call
 '   against a payKey, or a tracking id, or to specific receivers of a payKey or
 '   a tracking id that resulted from the Pay call.
 '
 '   A receiver itself with its own API credentials can make a Refund call against
 '   the transactionId corresponding to their transaction.
 '   The API credentials used in a Pay call cannot use transactionId to issue a refund
 '   for a transaction for which they themselves were not the receiver.
 '
 '   If you do specify specific receivers, you must provide the amounts as well.
 '   If you specify a transactionId, then only the receiver of that transactionId
 '   is affected. Therefore the receiverEmailArray and receiverAmountArray should
 '   have 1 entry each if you do want to give a partial refund.
 ' Inputs:
 '
 ' Conditionally Required:
 '  One of the following:  payKey or trackingId or trasactionId or
 '                         (payKey and receiverEmailArray and receiverAmountArray) or
 '                         (trackingId and receiverEmailArray and receiverAmountArray)
 '                         or (transactionId and receiverEmailArray
 '                         and receiverAmountArray)
 ' Returns:
 '  The NVP Collection object of the Refund call response.
 '------------------------------------------------------------------------------------
 */
 private function CallRefund( $payKey, $transactionId, $trackingId,
          $receiverEmailArray, $receiverAmountArray )
 {
  /* Gather the information to make the Refund call.
   The variable nvpstr holds the name-value pairs.
  */

  $nvpstr = "";

  // conditionally required fields
  if ("" != $payKey)
  {
   $nvpstr = "payKey=" . urlencode($payKey);
   if (0 != count($receiverEmailArray))
   {
    reset($receiverEmailArray);
    while (list($key, $value) = each($receiverEmailArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
     }
    }
   }
   if (0 != count($receiverAmountArray))
   {
    reset($receiverAmountArray);
    while (list($key, $value) = each($receiverAmountArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
     }
    }
   }
  }
  elseif ("" != $trackingId)
  {
   $nvpstr = "trackingId=" . urlencode($trackingId);
   if (0 != count($receiverEmailArray))
   {
    reset($receiverEmailArray);
    while (list($key, $value) = each($receiverEmailArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
     }
    }
   }
   if (0 != count($receiverAmountArray))
   {
    reset($receiverAmountArray);
    while (list($key, $value) = each($receiverAmountArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
     }
    }
   }
  }
  elseif ("" != $transactionId)
  {
   $nvpstr = "transactionId=" . urlencode($transactionId);
   // the caller should only have 1 entry in the email and amount arrays
   if (0 != count($receiverEmailArray))
   {
    reset($receiverEmailArray);
    while (list($key, $value) = each($receiverEmailArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
     }
    }
   }
   if (0 != count($receiverAmountArray))
   {
    reset($receiverAmountArray);
    while (list($key, $value) = each($receiverAmountArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
     }
    }
   }
  }

  /* Make the Refund call to PayPal */
  $resArray = $this->hash_call("Refund", $nvpstr);

  /* Return the response array */
  return $resArray;
 }

 /*
 '------------------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the PaymentDetails API Call.
 '   The PaymentDetails call can be made with either
 '   a payKey, a trackingId, or a transactionId of a previously successful Pay call.
 ' Inputs:
 '
 ' Conditionally Required:
 '  One of the following:  payKey or transactionId or trackingId
 ' Returns:
 '  The NVP Collection object of the PaymentDetails call response.
 '------------------------------------------------------------------------------------
 */
 private function CallPaymentDetails( $payKey, $transactionId, $trackingId )
 {
  /* Gather the information to make the PaymentDetails call.
   The variable nvpstr holds the name-value pairs.
  */

  $nvpstr = "";

  // conditionally required fields
  if ("" != $payKey)
  {
   $nvpstr = "payKey=" . urlencode($payKey);
  }
  elseif ("" != $transactionId)
  {
   $nvpstr = "transactionId=" . urlencode($transactionId);
  }
  elseif ("" != $trackingId)
  {
   $nvpstr = "trackingId=" . urlencode($trackingId);
  }

  /* Make the PaymentDetails call to PayPal */
  $resArray = $this->hash_call("PaymentDetails", $nvpstr);

  /* Return the response array */
  return $resArray;
 }

 /*
 '------------------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the Pay API Call.
 ' Inputs:
 '
 ' Required:
 '
 ' Optional:
 '
 ' Returns:
 '  The NVP Collection object of the Pay call response.
 '------------------------------------------------------------------------------------
 */
 private function CallPay( $actionType, $cancelUrl, $returnUrl, $currencyCode,
     $receiverEmailArray, $receiverAmountArray, $receiverPrimaryArray,
     $receiverInvoiceIdArray, $feesPayer, $ipnNotificationUrl, $memo,
     $pin, $preapprovalKey, $reverseAllParallelPaymentsOnError,
     $senderEmail, $trackingId )
 {

  /* Gather the information to make the Pay call.
   The variable nvpstr holds the name-value pairs.
  */



  // required fields
  $nvpstr = "actionType=" . urlencode($actionType) . "&currencyCode=";
  $nvpstr .= urlencode($currencyCode) . "&returnUrl=";
  $nvpstr .= urlencode($returnUrl) . "&cancelUrl=" . urlencode($cancelUrl);

  if (0 != count($receiverAmountArray))
  {

   reset($receiverAmountArray);

   while (list($key, $value) = each($receiverAmountArray))
   {
    if ("" != $value)
    {
     $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
    }
   }
  }

  if (0 != count($receiverEmailArray))
  {
   reset($receiverEmailArray);
   while (list($key, $value) = each($receiverEmailArray))
   {
    if ("" != $value)
    {
     $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
    }
   }
  }

  if (0 != count($receiverPrimaryArray))
  {
   reset($receiverPrimaryArray);
   while (list($key, $value) = each($receiverPrimaryArray))
   {
    if ("" != $value)
    {
     $nvpstr = $nvpstr . "&receiverList.receiver(" . $key . ").primary=" .
             urlencode($value);
    }
   }
  }

  if (0 != count($receiverInvoiceIdArray))
  {
   reset($receiverInvoiceIdArray);
   while (list($key, $value) = each($receiverInvoiceIdArray))
   {
    if ("" != $value)
    {
     $nvpstr = $nvpstr . "&receiverList.receiver(" . $key . ").invoiceId=" .
               urlencode($value);
    }
   }
  }

  // optional fields
  if ("" != $feesPayer)
  {
   $nvpstr .= "&feesPayer=" . urlencode($feesPayer);
  }

  if ("" != $ipnNotificationUrl)
  {
   $nvpstr .= "&ipnNotificationUrl=" . urlencode($ipnNotificationUrl);
  }

  if ("" != $memo)
  {
   $nvpstr .= "&memo=" . urlencode($memo);
  }

  if ("" != $pin)
  {
   $nvpstr .= "&pin=" . urlencode($pin);
  }

  if ("" != $preapprovalKey)
  {
   $nvpstr .= "&preapprovalKey=" . urlencode($preapprovalKey);
  }

  if ("" != $reverseAllParallelPaymentsOnError)
  {
   $nvpstr .= "&reverseAllParallelPaymentsOnError=";
   $nvpstr .= urlencode($reverseAllParallelPaymentsOnError);
  }

  if ("" != $senderEmail)
  {
   $nvpstr .= "&senderEmail=" . urlencode($senderEmail);
  }

  if ("" != $trackingId)
  {
   $nvpstr .= "&trackingId=" . urlencode($trackingId);
  }
//echo $nvpstr;exit;
  /* Make the Pay call to PayPal */
$resArray = $this->hash_call("Pay", $nvpstr);

  /* Return the response array */
  return $resArray;
 }

 /*
 '---------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the PreapprovalDetails API Call.
 ' Inputs:
 '

 ' Required:
 '  preapprovalKey:A preapproval key that identifies the agreement
 '                 resulting from a previously successful Preapproval call.
 ' Returns:
 '  The NVP Collection object of the PreapprovalDetails call response.
 '---------------------------------------------------------------------------
 */
 private function CallPreapprovalDetails( $preapprovalKey )
 {
  /* Gather the information to make the PreapprovalDetails call.
   The variable nvpstr holds the name-value pairs.
  */

  // required fields
  $nvpstr = "preapprovalKey=" . urlencode($preapprovalKey);

  /* Make the PreapprovalDetails call to PayPal */
  $resArray = $this->hash_call("PreapprovalDetails", $nvpstr);

  /* Return the response array */
  return $resArray;
 }

 /*
 '---------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the Preapproval API Call.
 ' Inputs:
 '
 ' Required:
 '
 ' Optional:
 '
 ' Returns:
 '  The NVP Collection object of the Preapproval call response.
 '---------------------------------------------------------------------------
 */
 private function CallPreapproval( $returnUrl, $cancelUrl, $currencyCode,
        $startingDate, $endingDate, $maxTotalAmountOfAllPayments,
        $senderEmail, $maxNumberOfPayments, $paymentPeriod, $dateOfMonth,
        $dayOfWeek, $maxAmountPerPayment, $maxNumberOfPaymentsPerPeriod, $pinType )
 {
  /* Gather the information to make the Preapproval call.
   The variable nvpstr holds the name-value pairs.
  */

  // required fields
  $nvpstr = "returnUrl=" . urlencode($returnUrl) . "&cancelUrl=" . urlencode($cancelUrl);
  $nvpstr .= "&currencyCode=" . urlencode($currencyCode) . "&startingDate=";
  $nvpstr .= urlencode($startingDate) . "&endingDate=" . urlencode($endingDate);
  $nvpstr .= "&maxTotalAmountOfAllPayments=" . urlencode($maxTotalAmountOfAllPayments);

  // optional fields
  if ("" != $senderEmail)
  {
   $nvpstr .= "&senderEmail=" . urlencode($senderEmail);
  }

  if ("" != $maxNumberOfPayments)
  {
   $nvpstr .= "&maxNumberOfPayments=" . urlencode($maxNumberOfPayments);
  }

  if ("" != $paymentPeriod)
  {
   $nvpstr .= "&paymentPeriod=" . urlencode($paymentPeriod);
  }

  if ("" != $dateOfMonth)
  {
   $nvpstr .= "&dateOfMonth=" . urlencode($dateOfMonth);
  }

  if ("" != $dayOfWeek)
  {
   $nvpstr .= "&dayOfWeek=" . urlencode($dayOfWeek);
  }

  if ("" != $maxAmountPerPayment)
  {
   $nvpstr .= "&maxAmountPerPayment=" . urlencode($maxAmountPerPayment);
  }

  if ("" != $maxNumberOfPaymentsPerPeriod)
  {
   $nvpstr .= "&maxNumberOfPaymentsPerPeriod=" . urlencode($maxNumberOfPaymentsPerPeriod);
  }

  if ("" != $pinType)
  {
   $nvpstr .= "&pinType=" . urlencode($pinType);
  }

  /* Make the Preapproval call to PayPal */
  $resArray = $this->hash_call("Preapproval", $nvpstr);

  /* Return the response array */
  return $resArray;
 }


private function callExecutepayment($payKey){
	  /* Make the Preapproval call to PayPal */
	  $nvpstr = "payKey=" . urlencode($payKey) ;
  $resArray = $this->hash_call("ExecutePayment", $nvpstr);

  /* Return the response array */
  return $resArray;
}
 /**
   '----------------------------------------------------------------------------------
   * hash_call: Function to perform the API call to PayPal using API signature
   * @methodName is name of API method.
   * @nvpStr is nvp string.
   * returns an associative array containing the response from the server.
   '----------------------------------------------------------------------------------
 */
 private function hash_call($methodName, $nvpStr)
 {
  //declaring of global variables
  //global $API_Endpoint, $API_UserName, $API_Password, $API_Signature, $API_AppID;
 // global $USE_PROXY, $PROXY_HOST, $PROXY_PORT;

  $API_Endpoint = $this->API_Endpoint."/" .$methodName;

  //setting the curl parameters.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);

  //turning off the server and peer verification(TrustManager Concept).
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch, CURLOPT_POST, 1);

  // Set the HTTP Headers
  curl_setopt($ch, CURLOPT_HTTPHEADER,  array(
  'X-PAYPAL-REQUEST-DATA-FORMAT: NV',
  'X-PAYPAL-RESPONSE-DATA-FORMAT: NV',
  'X-PAYPAL-SECURITY-USERID: ' . $this->API_UserName,
  'X-PAYPAL-SECURITY-PASSWORD: ' .$this->API_Password,
  'X-PAYPAL-SECURITY-SIGNATURE: ' . $this->API_Signature,
  'X-PAYPAL-SERVICE-VERSION: 1.3.0',
  'X-PAYPAL-APPLICATION-ID: ' . $this->API_AppID
 
  ));

    //if USE_PROXY constant set to TRUE in Constants.php,
    //then only proxy will be enabled.
  //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php
  if($this->USE_PROXY)
   curl_setopt ($ch, CURLOPT_PROXY, $this->PROXY_HOST. ":" . $this->PROXY_PORT);

  // RequestEnvelope fields
  $detailLevel = urlencode("ReturnAll"); // See DetailLevelCode in the WSDL
                                         // for valid enumerations
  $errorLanguage = urlencode("en_US");  // This should be the standard RFC
                                        // 3066 language identification tag,
                                        // e.g., en_US

 // NVPRequest for submitting to server
 $nvpreq = "requestEnvelope.errorLanguage=$errorLanguage&requestEnvelope";
 $nvpreq .= "detailLevel=$detailLevel&$nvpStr";
//echo $nvpreq;exit;
  //setting the nvpreq as POST FIELD to curl
  curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

  //getting response from server
  $response = curl_exec($ch);

  //converting NVPResponse to an Associative Array
  $nvpResArray=$this->deformatNVP($response);

  $nvpReqArray=$this->deformatNVP($nvpreq);

  $_SESSION['nvpReqArray']=$nvpReqArray;

  if (curl_errno($ch))
  {
   // moving to display page to display curl errors
     $_SESSION['curl_error_no']=curl_errno($ch) ;
     $_SESSION['curl_error_msg']=curl_error($ch);

     //Execute the Error handling module to display errors.
  }
  else
  {
    //closing the curl
    curl_close($ch);
  }

  return $nvpResArray;
 }

 /*'----------------------------------------------------------------------------
  Purpose: Redirects to PayPal.com site.
  Inputs:  $cmd is the querystring
  Returns:
 -------------------------------------------------------------------------------
 */
 private function RedirectToPayPal ( $cmd )
 {
  // Redirect to paypal.com here
 // global $Env;

  $payPalURL = "";

  if ($this->Env == "sandbox")
  {
   $payPalURL = "https://www.sandbox.paypal.com/webscr?" . $cmd;
  }
  else
  {
   $payPalURL = "https://www.paypal.com/webscr?" . $cmd;
  }
  
    echo '<script>location.href="'.$payPalURL.'"</script>';
  //header("Location: ".$payPalURL);
 }


 /*'----------------------------------------------------------------------------
   * This private function will take NVPString and convert it to an Associative Array
   * and then will decode the response.
   * It is useful to search for a particular key and display arrays.
   * @nvpstr is NVPString.
   * @nvpArray is Associative Array.
    ----------------------------------------------------------------------------
   */



 private function deformatNVP($nvpstr)
 {
  $intial=0;
  $nvpArray = array();

  while(strlen($nvpstr))
  {
   //postion of Key
   $keypos= strpos($nvpstr,'=');
   //position of value
   $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

   /*getting the Key and Value values and storing in a Associative Array*/
   $keyval=substr($nvpstr,$intial,$keypos);
   $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
   //decoding the respose
   $nvpArray[urldecode($keyval)] =urldecode( $valval);
   $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
      }
  return $nvpArray;
 }

}