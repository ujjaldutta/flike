<?php

class Ud_Paypaladaptive_Model_Observer {

    public function updatequantity(Varien_Event_Observer $observer) {

        $order = $observer->getEvent()->getOrder();


        $items = $order->getAllVisibleItems();
        foreach ($items as $item) {

            $orderedqty = $item->getQtyOrdered();
            $product = Mage::getModel("catalog/product")->load($item->getProductId());

            $oldqty = $product->getQuantityOrdered();
            $product->setQuantityOrdered($orderedqty + $oldqty);
            $product->save();

        }
	
	
	
	
	
	    $customer = Mage::getSingleton('customer/session')->getCustomer();
	    $customerEmail = $customer->getEmail();
	    $customerName = $customer->getName();
	   
	    $storename=Mage::getStoreConfig('trans_email/ident_general/name');  /* Sender Name */
	   
	    $storeemail=Mage::getStoreConfig('trans_email/ident_general/email'); /* Sender Email */
	    $emailTemplate  = Mage::getModel('core/email_template')
						->loadDefault('custom_email_after_order_place');
	   
	    $emailTemplate->setSenderEmail($customer->getEmail());
	    $emailTemplate->setSenderName($customer->getName());
	    
	   
	    $templateParams=array("order"=>$order,'customer'=>$customer);

	      //echo $processedTemplate = $emailTemplate->getProcessedTemplate($templateParams);
	   //   exit;
	   									
	   
	    $emailTemplate->send($storeemail,$storename, $templateParams);
	    $emailTemplate->send($customerEmail,$customerName, $templateParams);
	    

       
    }

}
