<?php
class Ud_Paypaladaptive_Model_Standard extends Mage_Payment_Model_Method_Abstract {
	protected $_code = 'paypaladaptive';
	
	protected $_isInitializeNeeded      = true;
	protected $_canUseInternal          = true;
	
        
          /**
     * Is this payment method a gateway (online auth/charge) ?
     */
    protected $_isGateway               = true;
 
    /**
     * Can authorize online?
     */
    protected $_canAuthorize            = true;
 
    /**
     * Can capture funds online?
     */
    protected $_canCapture              = true;
 
    /**
     * Can capture partial amounts online?
     */
    protected $_canCapturePartial       = false;
 
    /**
     * Can refund online?
     */
    protected $_canRefund               = false;
 
    /**
     * Can void transactions online?
     */
    protected $_canVoid                 = true;
 
  
 
    /**
     * Can show this payment method as an option on checkout payment page?
     */
    protected $_canUseCheckout          = true;
 
    /**
     * Is this payment method suitable for multi-shipping checkout?
     */
    protected $_canUseForMultishipping  = true;
 
    /**
     * Can save credit card information for future processing?
     */
    protected $_canSaveCc = false;
	
	public function getOrderPlaceRedirectUrl() {
		return Mage::getUrl('paypaladaptive/payment/redirect', array('_secure' => true));
	}
}
?>