<?php
class Ud_Paypaladaptive_Adminhtml_Model_System_Config_Source_Paypalenv{
	
	
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => "live", 'label'=>Mage::helper('adminhtml')->__('Paypal Live')),
            array('value' => "sandbox", 'label'=>Mage::helper('adminhtml')->__('Paypal Sandbox'))
           
        );
    }
	
}

?>