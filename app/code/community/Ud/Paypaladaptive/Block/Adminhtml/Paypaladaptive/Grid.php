<?php

class Ud_Paypaladaptive_Block_Adminhtml_Paypaladaptive_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("paypaladaptiveGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("paypaladaptive/paypaladaptive")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("paypaladaptive")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("order_id", array(
				"header" => Mage::helper("paypaladaptive")->__("Order Id"),
				"index" => "order_id",
				));
				$this->addColumn("transaction_id", array(
				"header" => Mage::helper("paypaladaptive")->__("Transaction Id"),
				"index" => "transaction_id",
				));
				$this->addColumn("paykey", array(
				"header" => Mage::helper("paypaladaptive")->__("Paykey"),
				"index" => "paykey",
				));
                                $this->addColumn("preapprovalKey", array(
				"header" => Mage::helper("paypaladaptive")->__("PreApprovalKey"),
				"index" => "preapprovalKey",
				));
					$this->addColumn('create_date', array(
						'header'    => Mage::helper('paypaladaptive')->__('Transaction Date'),
						'index'     => 'create_date',
						'type'      => 'datetime',
					));
						$this->addColumn('status', array(
						'header' => Mage::helper('paypaladaptive')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Ud_Paypaladaptive_Block_Adminhtml_Paypaladaptive_Grid::getOptionArray4(),				
						));
						 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return '#';
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_paypaladaptive', array(
					 'label'=> Mage::helper('paypaladaptive')->__('Remove Paypaladaptive'),
					 'url'  => $this->getUrl('*/adminhtml_paypaladaptive/massRemove'),
					 'confirm' => Mage::helper('paypaladaptive')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]='pending';
			$data_array[1]='complete';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Ud_Paypaladaptive_Block_Adminhtml_Paypaladaptive_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}