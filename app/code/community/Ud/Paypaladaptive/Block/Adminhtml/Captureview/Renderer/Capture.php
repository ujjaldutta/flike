<?php
class Ud_Paypaladaptive_Block_Adminhtml_Capturepayment_Renderer_Capture extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
    
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        
        $id    =  $row->getId();
        
        if( $value == "0"){
            
            return '<a href="'.$this->getUrl('paypaladaptive/adminhtml_capturepayment/capture', array('id'=> $id)).'" >Capture</a>';
            
        }else if( $value == "1" ){
            
            return '<a href="'.$this->getUrl('paypaladaptive/adminhtml_capturepayment/view', array('id'=> $id)).'" >View</a>';
            
        }
        
    }
}