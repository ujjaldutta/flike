<?php

class Ud_Paypaladaptive_Block_Adminhtml_Captureview_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
                    parent::__construct();
                    $this->setId("captureviewGrid");
                    //$this->setDefaultSort("id");
                    $this->setDefaultDir("ASC");
                    $this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
                    $product_id = $this->getRequest()->getParam('id');
                    $collection = Mage::getModel('paypaladaptive/capturepayment')->getCollection()->addFieldToFilter('product_id', $product_id);
                    
                    //$collection->addAttributeToSelect('*');
                    $this->setCollection($collection);
                    
                    return parent::_prepareCollection();
		}
                
                
		protected function _prepareColumns()
		{
                        $this->addColumn('id', array(
                            'header'    =>  Mage::helper('paypaladaptive')->__('ID'),
                            'align'     => 'right',
                            'width'     => '10px',
                            'index'     => 'id',
                          ));

                          $this->addColumn('customer_email', array(
                            'header'    => Mage::helper('paypaladaptive')->__('Customer Email'),
                            'align'     => 'left',
                            'index'     => 'name',
                            'width'     => '50px',
                            'index'     => 'customer_email',
                          ));

                          $this->addColumn('transaction_id', array(
                            'header'    => Mage::helper('paypaladaptive')->__('Transaction ID'),
                            'align'     => 'right',
                            'width'     => '10px',
                            'index'     => 'transaction_id',
                          ));
                          
                           $this->addColumn('paykey', array(
                            'header'    => Mage::helper('paypaladaptive')->__('Pay Key'),
                            'align'     => 'right',
                            'width'     => '10px',
                            'index'     => 'paykey',
                          ));
                           
                           $this->addColumn('created_at', array(
                            'header'    => Mage::helper('paypaladaptive')->__('On'),
                            'align'     => 'right',
                            'width'     => '10px',
                            'index'     => 'created_at',
                          ));
                           
                          $this->addColumn('action',
                            array(
                                'header' => Mage::helper('catalog/product')->__('Action'),
                                'width' => '100',
                                'type' => 'action',
                                'getter' => 'getId',
                                'actions' => array(
                                       array(
                                            'caption' => Mage::helper('paypaladaptive')->__('Refund'),
                                            'url' => array('base'=> '*/*/refund'),
                                            'field' => 'id'
                                          )),
                                'filter' => false,
                                'sortable' => false,
                                'index' => 'stores',
                                'is_system' => true,
                            ));
                          
                          
//                          $this->addColumn('action',
//                            array(
//                            'header'=> Mage::helper('catalog')->__('Action'),
//                            'index' => 'is_captured',
//                            'width'     => '100px',
//                            'renderer'  => 'Ud_Paypaladaptive_Block_Adminhtml_Capturepayment_Renderer_Capture',// THIS IS WHAT THIS POST IS ALL ABOUT
//                            ));
                          
                          return parent::_prepareColumns();

		}

		public function getRowUrl($row)
		{
			   return '#';
		}


		
		protected function _prepareMassaction()
		{
//			$this->setMassactionIdField('id');
//			$this->getMassactionBlock()->setFormFieldName('ids');
//			$this->getMassactionBlock()->setUseSelectAll(true);
//			$this->getMassactionBlock()->addItem('remove_paypaladaptive', array(
//					 'label'=> Mage::helper('paypaladaptive')->__('Remove Paypaladaptive'),
//					 'url'  => $this->getUrl('*/adminhtml_paypaladaptive/massRemove'),
//					 'confirm' => Mage::helper('paypaladaptive')->__('Are you sure?')
//				));
//			return $this;
		}
			
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]='pending';
			$data_array[1]='complete';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Ud_Paypaladaptive_Block_Adminhtml_Paypaladaptive_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}