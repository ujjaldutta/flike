<?php


class Ud_Paypaladaptive_Block_Adminhtml_Captureview extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{
            $this->_controller = "adminhtml_captureview";
            $this->_blockGroup = "paypaladaptive";
            $this->_headerText = Mage::helper("paypaladaptive")->__("Captured Payment");
            $this->_addButtonLabel = Mage::helper("paypaladaptive")->__("Add New Item");
            parent::__construct();
            $this->_removeButton('add');
	}

}