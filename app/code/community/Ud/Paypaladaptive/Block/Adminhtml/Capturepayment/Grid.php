<?php

class Ud_Paypaladaptive_Block_Adminhtml_Capturepayment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
                    parent::__construct();
                    $this->setId("capturepaymentGrid");
                    //$this->setDefaultSort("id");
                    $this->setDefaultDir("ASC");
                    $this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
                    $helper            = Mage::helper('campaign');
                    $successfull_option_id = $helper -> getOptionId('campaign_status','Successfull');
                    $collection = Mage::getModel('catalog/product')->getCollection()->setOrder('created_at', 'ASC')->addFieldToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)->addFieldToFilter('status', "1")->addFieldToFilter('campaign_status', $successfull_option_id);  
                    
                    $collection->addAttributeToSelect('*');
                    $this->setCollection($collection);
                    
                    return parent::_prepareCollection();
		}
                
                
		protected function _prepareColumns()
		{
                        $this->addColumn('name', array(
                            'header'    => Mage::helper('catalog/product')->__('Campaign Name'),
                            'align'     => 'right',
                            'width'     => '10px',
                            'index'     => 'name',
                          ));

//                          $this->addColumn('user_id', array(
//                            'header'    => Mage::helper('catalog/product')->__('Designer Name'),
//                            'align'     => 'left',
//                            'index'     => 'name',
//                            'width'     => '50px',
//                            'index'     => 'user_id',
//                          ));


                          $this->addColumn('sku', array(
                              'header'    => Mage::helper('catalog/product')->__('SKU'),
                              'width'     => '150px',
                              'index'     => 'sku',
                          ));
                          
//                          $this->addColumn('action',
//                            array(
//                                'header' => Mage::helper('catalog/product')->__('Action'),
//                                'width' => '100',
//                                'type' => 'action',
//                                'getter' => 'getId',
//                                'actions' => array(
//                                       array(
//                                            'caption' => Mage::helper('catalog/product')->__('Capture'),
//                                            'url' => array('base'=> '*/*/capture'),
//                                            'field' => 'id'
//                                          )),
//                                'filter' => false,
//                                'sortable' => false,
//                                'index' => 'stores',
//                                'is_system' => true,
//                            ));
                          
                          
                          $this->addColumn('action',
                            array(
                            'header'=> Mage::helper('catalog')->__('Action'),
                            'index' => 'is_captured',
                            'filter' => false,
                            'width'     => '100px',
                            'renderer'  => 'Ud_Paypaladaptive_Block_Adminhtml_Capturepayment_Renderer_Capture',// THIS IS WHAT THIS POST IS ALL ABOUT
                            ));
                          
                          return parent::_prepareColumns();

		}

		public function getRowUrl($row)
		{
			   return '#';
		}


		
		protected function _prepareMassaction()
		{
//			$this->setMassactionIdField('id');
//			$this->getMassactionBlock()->setFormFieldName('ids');
//			$this->getMassactionBlock()->setUseSelectAll(true);
//			$this->getMassactionBlock()->addItem('remove_paypaladaptive', array(
//					 'label'=> Mage::helper('paypaladaptive')->__('Remove Paypaladaptive'),
//					 'url'  => $this->getUrl('*/adminhtml_paypaladaptive/massRemove'),
//					 'confirm' => Mage::helper('paypaladaptive')->__('Are you sure?')
//				));
//			return $this;
		}
			
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]='pending';
			$data_array[1]='complete';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Ud_Paypaladaptive_Block_Adminhtml_Paypaladaptive_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}