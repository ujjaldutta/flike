<?php


class Ud_Paypaladaptive_Block_Adminhtml_Capturepayment extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{
            $this->_controller = "adminhtml_capturepayment";
            $this->_blockGroup = "paypaladaptive";
            $this->_headerText = Mage::helper("paypaladaptive")->__("Capture Payment");
            $this->_addButtonLabel = Mage::helper("paypaladaptive")->__("Add New Item");
            parent::__construct();
            $this->_removeButton('add');
	}

}