<?php

class Ud_Paypaladaptive_Adminhtml_CapturepaymentController extends Mage_Adminhtml_Controller_Action{
   
    
/********************************************
 PayPal Adaptive Payments API Module
 
 Defines all the global variables and the wrapper functions
 ********************************************/
private $PROXY_HOST;
private $PROXY_PORT;

private $Env;

 //------------------------------------
 // PayPal API Credentials
 // Replace <API_USERNAME> with your API Username
 // Replace <API_PASSWORD> with your API Password
 // Replace <API_SIGNATURE> with your Signature
 //------------------------------------
private $API_UserName;
private $API_Password;
private $API_Signature;

private $API_AppID;

private $API_Endpoint;

private  $USE_PROXY;
    
    
    
    
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("paypaladaptive/paypaladaptive")->_addBreadcrumb(Mage::helper("adminhtml")->__("Paypaladaptive  Manager"),Mage::helper("adminhtml")->__("Paypaladaptive Manager"));
        return $this;
    }
    
    public function indexAction() 
    {
        $this->_title($this->__("Paypaladaptive"));
        $this->_title($this->__("Capture Payment"));

        $this->_initAction();
        $this->renderLayout();
    }
    
    public function captureAction(){
        
        
        $this->PROXY_HOST='127.0.0.1';
        $this->PROXY_PORT='808';
        $en = Mage::getStoreConfig('payment/paypaladaptive/paypal_env');
        if($en=='0'){
            $this->Env='sandbox';
        }else{
            $this->Env='live';
        }

        $this->API_UserName  = Mage::getStoreConfig('payment/paypaladaptive/apiuser');
        $this->API_Password  = Mage::getStoreConfig('payment/paypaladaptive/apipassword');
        $this->API_Signature = Mage::getStoreConfig('payment/paypaladaptive/apisignature');
        $this->API_AppID     = Mage::getStoreConfig('payment/paypaladaptive/api_appid');

         if ($this->Env == "sandbox")
                  $this->API_Endpoint='https://svcs.sandbox.paypal.com/AdaptivePayments';
         else
                  $this->API_Endpoint='https://svcs.paypal.com/AdaptivePayments';



        $this->USE_PROXY=false;
        
        
        
        
        
        
        $id = $this->getRequest()->getParam('id');
        
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
       // $sql        = "SELECT item_id, order_id, quote_item_id, product_id, base_row_total_incl_tax  FROM sales_flat_order_item WHERE product_id = '".$id."'";
      //  $all_order  = $connection->fetchAll($sql);
       
        $sql = "SELECT si.item_id, si.order_id, si.quote_item_id, si.product_id, si.base_row_total_incl_tax, so.increment_id, so.customer_email, so.status FROM sales_flat_order_item as si INNER JOIN sales_flat_order as so ON(si.order_id = so.entity_id && so.status = 'processing')"
              . " WHERE si.product_id = '".$id."'";
        
        $all_order  = $connection->fetchAll( $sql );
        foreach( $all_order as $key => $value ){
            
            $orderid    = $value['order_id'];
           // $order           = Mage::getModel('sales/order')->load($orderid);
            //$orderid = "115";
           // $sql2        = "SELECT increment_id, customer_email, status  FROM sales_flat_order WHERE entity_id = '".$orderid."' and status = 'processing'";
           // $order  = $connection->fetchRow($sql2);
            
            $Incrementid     = $value['increment_id'];
            $customer_email  = $value['customer_email'];
            $status          = $value['status'];
           // echo 'Increment Id : '.$Incrementid.'<br>';
            //echo 'Status : '.$status.'<br>';
            //if( $Incrementid != "" &&  $status == "processing"){
                
           
            $sql1             = "SELECT id, order_id, preapprovalKey, status, paykey, senderEmail  FROM paypaladaptive_transaction WHERE order_id = '".$Incrementid."' and status = 'complete'";
            
            $payment_detail  = $connection->fetchRow($sql1);
           //print_r($payment_detail);
            if( count( $payment_detail ) > 0 ){
             
             
             $preapprovalKey  = $payment_detail['preapprovalKey'];
            $orderid         = $payment_detail['order_id'];
            
           $actionType   = "PAY";
           $cancelUrl    = Mage::getUrl('paypaladaptive/payment/cancel');
           $returnUrl    = Mage::getUrl('paypaladaptive/payment/response');
           $currencyCode = "EUR";
           
           $receiverEmailArray = array(
			 Mage::getStoreConfig('payment/paypaladaptive/merchantemail')
			  );
           
           $amount = $value['base_row_total_incl_tax'];
          
         /* $receiverAmountArray = array(
			  ($amount-($amount*.90)),
			  ($amount-($amount*.10))
			  );*/
           
           $receiverAmountArray = array( $amount  );
           
          // Request specific optional fields
            //   Provide a value for each field that you want to include in the request;
            //   if left as an empty string, the field will not be passed in the request
            $senderEmail = $payment_detail['senderEmail']; // TODO - If you are executing the Pay call against a preapprovalKey,
                                               // you should set senderEmail
            $receiverInvoiceIdArray = array( $Incrementid );

            $receiverPrimaryArray = array(

                                      );// It is not required if the web approval flow immediately
                                                                       // follows this Pay call
            $feesPayer = "";
            $ipnNotificationUrl = "";
            $memo = ""; // maxlength is 1000 characters
            $pin = ""; // TODO - If you are executing the Pay call against an existing preapproval
                               // that requires a pin, then you must set this
            $preapprovalKey = $preapprovalKey; // TODO - If you are executing the Pay call against an existing
                                                      // preapproval, set the preapprovalKey here
            $reverseAllParallelPaymentsOnError = ""; // TODO - Do not specify for chained payment
            $trackingId = $this->generateTrackingID(); // generateTrackingID function is found
                                                                                    // in paypalplatform.php 
            
           $resArray =  $this -> CallPay($actionType, $cancelUrl, $returnUrl, $currencyCode,
                            $receiverEmailArray, $receiverAmountArray, $receiverPrimaryArray,
                            $receiverInvoiceIdArray, $feesPayer, $ipnNotificationUrl, $memo,
                            $pin, $preapprovalKey, $reverseAllParallelPaymentsOnError,
                            $senderEmail, $trackingId );
           //print '<pre>';
           //print_r($resArray);
          /* if(!is_array($resArray) || $resArray == ""){
               continue;
           }*/
          /* print '<pre>';
           print_r($resArray);exit;*/
           $ack = strtoupper($resArray["responseEnvelope.ack"]);
           
          if($ack=="SUCCESS") {
               
              $transactionId  =  $resArray['paymentInfoList.paymentInfo(0).transactionId'];
              $paykey         =  $resArray['payKey'];
              $preapprovalKey =  $resArray['preapprovalKey'];
               
               $write_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
               $query = "INSERT INTO paypaladaptive_captured SET order_id = '".$orderid."', transaction_id = '".$transactionId."', paykey = '".$paykey."', preapprovalKey = '".$preapprovalKey."', product_id = '".$id."', customer_email = '".$customer_email."', status = 'captured'";
               $write_connection->query($query);
               
           }else{
               
                $ErrorCode = urldecode($resArray["error(0).errorId"]);
                $ErrorMsg = urldecode($resArray["error(0).message"]);
                $ErrorDomain = urldecode($resArray["error(0).domain"]);
                $ErrorSeverity = urldecode($resArray["error(0).severity"]);
                $ErrorCategory = urldecode($resArray["error(0).category"]);

                echo "Pay API call failed. ";
                echo "Detailed Error Message: " . $ErrorMsg;
                echo "Error Code: " . $ErrorCode;
                echo "Error Severity: " . $ErrorSeverity;
                echo "Error Domain: " . $ErrorDomain;
                echo "Error Category: " . $ErrorCategory;
                
          }
        }
        //}
        }
        //exit;
        //product update code
            $data['is_captured'] = 1;
            $product = Mage::getModel('catalog/product')->load($id)->addData($data);
            $product ->setId($id)->save();
           
         Mage::getSingleton('core/session')->addSuccess('Payment Captured Successfully');  
         $this->_redirect('paypaladaptive/adminhtml_capturepayment/index');
        //end
    }
    
    public function dorefundAction(){
        
        $captured_id    = $this->getRequest()->getPost('captured_id');
        $transaction_id = $this->getRequest()->getPost('transaction_id');
        $paykey         = $this->getRequest()->getPost('paykey');
        $amount         = $this->getRequest()->getPost('amount');
        $order_id       = $this->getRequest()->getPost('order_id');
      
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "SELECT * FROM paypaladaptive_transaction  WHERE order_id = '".$order_id."'";
        
        $transact    = $connection->fetchRow( $sql );
        $senderEmail = $transact['senderEmail'];
        $trackingId  = $transact['trackingId'];
        
        $this->PROXY_HOST='127.0.0.1';
        $this->PROXY_PORT='808';
        
        $en = Mage::getStoreConfig('payment/paypaladaptive/paypal_env');
        if($en == '0'){
            $this->Env='sandbox';
        }else{
            $this->Env='live';
        }

        $this->API_UserName  = Mage::getStoreConfig('payment/paypaladaptive/apiuser');
        $this->API_Password  = Mage::getStoreConfig('payment/paypaladaptive/apipassword');
        $this->API_Signature = Mage::getStoreConfig('payment/paypaladaptive/apisignature');
        $this->API_AppID     = Mage::getStoreConfig('payment/paypaladaptive/api_appid');

         if ($this->Env == "sandbox")
                  $this->API_Endpoint='https://svcs.sandbox.paypal.com/AdaptivePayments';
         else
                  $this->API_Endpoint='https://svcs.paypal.com/AdaptivePayments';



        $this->USE_PROXY=false;
        
        
        $receiverAmountArray = array( $amount  );
         $payKey = $paykey;
        $transactionId = $transaction_id;
        //$trackingId = "";
        
        $receiverEmailArray = array( $senderEmail );
        $currencyCode = "EUR";
        $resArray =  $this -> CallRefund( $payKey, $transactionId, $trackingId,
                                    $receiverEmailArray, $receiverAmountArray, $currencyCode );
           /* print '<pre>';
          print_r($resArray);
          exit;
          if(!is_array($resArray) || $resArray == ""){
               continue;
           }*/
            $ack = strtoupper($resArray["responseEnvelope.ack"]);
            $refund_status  =  $resArray['refundInfoList.refundInfo(0).refundStatus'];
            $refund_amount  =  $resArray['refundInfoList.refundInfo(0).refundNetAmount'];
            $refund_true    =  $resArray['refundInfoList.refundInfo(0).refundHasBecomeFull'];
        
            //exit;
          if($ack == "SUCCESS" && ($refund_status == "REFUNDED" || $refund_status == "PREVIOUS_REFUND_PENDING" )) {
               
              
              
              
               $write_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
               $query = "INSERT INTO paypaladaptive_refund SET captured_id = '".$captured_id."', amount = '".$amount."'";
               $write_connection->query($query);
               
               Mage::getSingleton('core/session')->addSuccess('Payment Refunded Successfully');
               if($refund_status == "PREVIOUS_REFUND_PENDING")
               Mage::getSingleton('core/session')->addError('Previous refund is pending'); 
               $this->_redirect('paypaladaptive/adminhtml_capturepayment/index');
               
           }else{
               
                $ErrorCode = urldecode($resArray["error(0).errorId"]);
                $ErrorMsg = urldecode($resArray["error(0).message"]);
                $ErrorDomain = urldecode($resArray["error(0).domain"]);
                $ErrorSeverity = urldecode($resArray["error(0).severity"]);
                $ErrorCategory = urldecode($resArray["error(0).category"]);

                echo "Pay API call failed. ";
                echo "Detailed Error Message: " . $ErrorMsg;
                echo "Error Code: " . $ErrorCode;
                echo "Error Severity: " . $ErrorSeverity;
                echo "Error Domain: " . $ErrorDomain;
                echo "Error Category: " . $ErrorCategory;
                echo "Refund Status : ".$refund_status;
                
          }
        
    }
    
    
    //payment related functions
    
    
private function generateCharacter () {
  $possible = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
  return $char;
 }

 private function generateTrackingID () {
 $GUID = $this->generateCharacter().$this->generateCharacter().$this->generateCharacter();
 $GUID .=$this->generateCharacter().$this->generateCharacter().$this->generateCharacter();
 $GUID .=$this->generateCharacter().$this->generateCharacter().$this->generateCharacter();
 return $GUID;
 }

 /*
 '-------------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the Refund API Call.
 '   The API credentials used in a Pay call can make the Refund call
 '   against a payKey, or a tracking id, or to specific receivers of a payKey or
 '   a tracking id that resulted from the Pay call.
 '
 '   A receiver itself with its own API credentials can make a Refund call against
 '   the transactionId corresponding to their transaction.
 '   The API credentials used in a Pay call cannot use transactionId to issue a refund
 '   for a transaction for which they themselves were not the receiver.
 '
 '   If you do specify specific receivers, you must provide the amounts as well.
 '   If you specify a transactionId, then only the receiver of that transactionId
 '   is affected. Therefore the receiverEmailArray and receiverAmountArray should
 '   have 1 entry each if you do want to give a partial refund.
 ' Inputs:
 '
 ' Conditionally Required:
 '  One of the following:  payKey or trackingId or trasactionId or
 '                         (payKey and receiverEmailArray and receiverAmountArray) or
 '                         (trackingId and receiverEmailArray and receiverAmountArray)
 '                         or (transactionId and receiverEmailArray
 '                         and receiverAmountArray)
 ' Returns:
 '  The NVP Collection object of the Refund call response.
 '------------------------------------------------------------------------------------
 */
 private function CallRefund( $payKey, $transactionId, $trackingId,
          $receiverEmailArray, $receiverAmountArray, $currencyCode )
 {
  /* Gather the information to make the Refund call.
   The variable nvpstr holds the name-value pairs.
  */

  $nvpstr = "";

  // conditionally required fields
  if("" != $currencyCode){
      $nvpstr .= "currencyCode=" . urlencode($currencyCode);
  }
  
  if($payKey){
      $nvpstr .= "&payKey=" . urlencode($payKey);
  }
  /*if ("" != $payKey)
  {
   $nvpstr .= "payKey=" . urlencode($payKey);
   if (0 != count($receiverEmailArray))
   {
    reset($receiverEmailArray);
    while (list($key, $value) = each($receiverEmailArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
     }
    }
   }
   if (0 != count($receiverAmountArray))
   {
    reset($receiverAmountArray);
    while (list($key, $value) = each($receiverAmountArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
     }
    }
   }
  }*/
  /*elseif ("" != $trackingId)
  {
   $nvpstr = "trackingId=" . urlencode($trackingId);
   if (0 != count($receiverEmailArray))
   {
    reset($receiverEmailArray);
    while (list($key, $value) = each($receiverEmailArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
     }
    }
   }
   if (0 != count($receiverAmountArray))
   {
    reset($receiverAmountArray);
    while (list($key, $value) = each($receiverAmountArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
     }
    }
   }
  }*/
  if ("" != $transactionId)
  {
   $nvpstr .= "&transactionId=" . urlencode($transactionId);
   // the caller should only have 1 entry in the email and amount arrays
   if (0 != count($receiverEmailArray))
   {
    reset($receiverEmailArray);
    while (list($key, $value) = each($receiverEmailArray))
    {
     if ("" != $value)
     {
      $nvpstr .= "&email=" . urlencode($value);
     // $nvpstr .= "&email=amal.unified100@gmail.com";
     }
    }
   }
   if (0 != count($receiverAmountArray))
   {
    reset($receiverAmountArray);
    while (list($key, $value) = each($receiverAmountArray))
    {
     if ("" != $value)
     {
      //$nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
         $nvpstr .= "&amount=" . urlencode($value);
     }
    }
   }
  }
//echo $nvpstr;exit;
  /* Make the Refund call to PayPal */
  $resArray = $this->hash_call("Refund", $nvpstr);

  /* Return the response array */
  return $resArray;
 }

 /*
 '------------------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the PaymentDetails API Call.
 '   The PaymentDetails call can be made with either
 '   a payKey, a trackingId, or a transactionId of a previously successful Pay call.
 ' Inputs:
 '
 ' Conditionally Required:
 '  One of the following:  payKey or transactionId or trackingId
 ' Returns:
 '  The NVP Collection object of the PaymentDetails call response.
 '------------------------------------------------------------------------------------
 */
 private function CallPaymentDetails( $payKey, $transactionId='', $trackingId='' )
 {
  /* Gather the information to make the PaymentDetails call.
   The variable nvpstr holds the name-value pairs.
  */

  $nvpstr = "";

  // conditionally required fields
  if ("" != $payKey)
  {
   $nvpstr = "payKey=" . urlencode($payKey);
  }
  elseif ("" != $transactionId)
  {
   $nvpstr = "transactionId=" . urlencode($transactionId);
  }
  elseif ("" != $trackingId)
  {
   $nvpstr = "trackingId=" . urlencode($trackingId);
  }

  /* Make the PaymentDetails call to PayPal */
  $resArray = $this->hash_call("PaymentDetails", $nvpstr);

  /* Return the response array */
  return $resArray;
 }

 /*
 '------------------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the Pay API Call.
 ' Inputs:
 '
 ' Required:
 '
 ' Optional:
 '
 ' Returns:
 '  The NVP Collection object of the Pay call response.
 '------------------------------------------------------------------------------------
 */
 private function CallPay( $actionType, $cancelUrl, $returnUrl, $currencyCode,
     $receiverEmailArray, $receiverAmountArray, $receiverPrimaryArray,
     $receiverInvoiceIdArray, $feesPayer, $ipnNotificationUrl, $memo,
     $pin, $preapprovalKey, $reverseAllParallelPaymentsOnError,
     $senderEmail, $trackingId )
 {

  /* Gather the information to make the Pay call.
   The variable nvpstr holds the name-value pairs.
  */



  // required fields
  $nvpstr = "actionType=" . urlencode($actionType) . "&currencyCode=";
  $nvpstr .= urlencode($currencyCode) . "&returnUrl=";
  $nvpstr .= urlencode($returnUrl) . "&cancelUrl=" . urlencode($cancelUrl);

  if (0 != count($receiverAmountArray))
  {

   reset($receiverAmountArray);

   while (list($key, $value) = each($receiverAmountArray))
   {
    if ("" != $value)
    {
     $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
    }
   }
  }

  if (0 != count($receiverEmailArray))
  {
   reset($receiverEmailArray);
   while (list($key, $value) = each($receiverEmailArray))
   {
    if ("" != $value)
    {
     $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
    }
   }
  }

  if (0 != count($receiverPrimaryArray))
  {
   reset($receiverPrimaryArray);
   while (list($key, $value) = each($receiverPrimaryArray))
   {
    if ("" != $value)
    {
     $nvpstr = $nvpstr . "&receiverList.receiver(" . $key . ").primary=" .
             urlencode($value);
    }
   }
  }

  if (0 != count($receiverInvoiceIdArray))
  {
   reset($receiverInvoiceIdArray);
   while (list($key, $value) = each($receiverInvoiceIdArray))
   {
    if ("" != $value)
    {
     $nvpstr = $nvpstr . "&receiverList.receiver(" . $key . ").invoiceId=" .
               urlencode($value);
    }
   }
  }

  // optional fields
  if ("" != $feesPayer)
  {
   $nvpstr .= "&feesPayer=" . urlencode($feesPayer);
  }

  if ("" != $ipnNotificationUrl)
  {
   $nvpstr .= "&ipnNotificationUrl=" . urlencode($ipnNotificationUrl);
  }

  if ("" != $memo)
  {
   $nvpstr .= "&memo=" . urlencode($memo);
  }

  if ("" != $pin)
  {
   $nvpstr .= "&pin=" . urlencode($pin);
  }

  if ("" != $preapprovalKey)
  {
   $nvpstr .= "&preapprovalKey=" . urlencode($preapprovalKey);
  }

  if ("" != $reverseAllParallelPaymentsOnError)
  {
   $nvpstr .= "&reverseAllParallelPaymentsOnError=";
   $nvpstr .= urlencode($reverseAllParallelPaymentsOnError);
  }

  if ("" != $senderEmail)
  {
   $nvpstr .= "&senderEmail=" . urlencode($senderEmail);
  }

  if ("" != $trackingId)
  {
   $nvpstr .= "&trackingId=" . urlencode($trackingId);
  }
//echo $nvpstr;exit;
  /* Make the Pay call to PayPal */
$resArray = $this->hash_call("Pay", $nvpstr);

  /* Return the response array */
  return $resArray;
 }

 /*
 '---------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the PreapprovalDetails API Call.
 ' Inputs:
 '

 ' Required:
 '  preapprovalKey:A preapproval key that identifies the agreement
 '                 resulting from a previously successful Preapproval call.
 ' Returns:
 '  The NVP Collection object of the PreapprovalDetails call response.
 '---------------------------------------------------------------------------
 */
 private function CallPreapprovalDetails( $preapprovalKey )
 {
  /* Gather the information to make the PreapprovalDetails call.
   The variable nvpstr holds the name-value pairs.
  */

  // required fields
  $nvpstr = "preapprovalKey=" . urlencode($preapprovalKey);

  /* Make the PreapprovalDetails call to PayPal */
  $resArray = $this->hash_call("PreapprovalDetails", $nvpstr);

  /* Return the response array */
  return $resArray;
 }

 /*
 '---------------------------------------------------------------------------
 ' Purpose: Prepares the parameters for the Preapproval API Call.
 ' Inputs:
 '
 ' Required:
 '
 ' Optional:
 '
 ' Returns:
 '  The NVP Collection object of the Preapproval call response.
 '---------------------------------------------------------------------------
 */
 private function CallPreapproval( $returnUrl, $cancelUrl, $currencyCode,
        $startingDate, $endingDate, $maxTotalAmountOfAllPayments,
        $senderEmail, $maxNumberOfPayments, $paymentPeriod, $dateOfMonth,
        $dayOfWeek, $maxAmountPerPayment, $maxNumberOfPaymentsPerPeriod, $pinType )
 {
  /* Gather the information to make the Preapproval call.
   The variable nvpstr holds the name-value pairs.
  */

  // required fields
  $nvpstr = "returnUrl=" . urlencode($returnUrl) . "&cancelUrl=" . urlencode($cancelUrl);
  $nvpstr .= "&currencyCode=" . urlencode($currencyCode) . "&startingDate=";
  $nvpstr .= urlencode($startingDate) . "&endingDate=" . urlencode($endingDate);
  $nvpstr .= "&maxTotalAmountOfAllPayments=" . urlencode($maxTotalAmountOfAllPayments);

  // optional fields
  if ("" != $senderEmail)
  {
   $nvpstr .= "&senderEmail=" . urlencode($senderEmail);
  }

  if ("" != $maxNumberOfPayments)
  {
   $nvpstr .= "&maxNumberOfPayments=" . urlencode($maxNumberOfPayments);
  }

  if ("" != $paymentPeriod)
  {
   $nvpstr .= "&paymentPeriod=" . urlencode($paymentPeriod);
  }

  if ("" != $dateOfMonth)
  {
   $nvpstr .= "&dateOfMonth=" . urlencode($dateOfMonth);
  }

  if ("" != $dayOfWeek)
  {
   $nvpstr .= "&dayOfWeek=" . urlencode($dayOfWeek);
  }

  if ("" != $maxAmountPerPayment)
  {
   $nvpstr .= "&maxAmountPerPayment=" . urlencode($maxAmountPerPayment);
  }

  if ("" != $maxNumberOfPaymentsPerPeriod)
  {
   $nvpstr .= "&maxNumberOfPaymentsPerPeriod=" . urlencode($maxNumberOfPaymentsPerPeriod);
  }

  if ("" != $pinType)
  {
   $nvpstr .= "&pinType=" . urlencode($pinType);
  }

  /* Make the Preapproval call to PayPal */
  $resArray = $this->hash_call("Preapproval", $nvpstr);

  /* Return the response array */
  return $resArray;
 }


private function callExecutepayment($payKey){
	  /* Make the Preapproval call to PayPal */
	  $nvpstr = "payKey=" . urlencode($payKey) ;
  $resArray = $this->hash_call("ExecutePayment", $nvpstr);

  /* Return the response array */
  return $resArray;
}
 /**
   '----------------------------------------------------------------------------------
   * hash_call: Function to perform the API call to PayPal using API signature
   * @methodName is name of API method.
   * @nvpStr is nvp string.
   * returns an associative array containing the response from the server.
   '----------------------------------------------------------------------------------
 */
 private function hash_call($methodName, $nvpStr)
 {
  //declaring of global variables
  //global $API_Endpoint, $API_UserName, $API_Password, $API_Signature, $API_AppID;
 // global $USE_PROXY, $PROXY_HOST, $PROXY_PORT;

  $API_Endpoint = $this->API_Endpoint."/" .$methodName;

  //setting the curl parameters.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);

  //turning off the server and peer verification(TrustManager Concept).
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch, CURLOPT_POST, 1);

  // Set the HTTP Headers
  curl_setopt($ch, CURLOPT_HTTPHEADER,  array(
  'X-PAYPAL-REQUEST-DATA-FORMAT: NV',
  'X-PAYPAL-RESPONSE-DATA-FORMAT: NV',
  'X-PAYPAL-SECURITY-USERID: ' . $this->API_UserName,
  'X-PAYPAL-SECURITY-PASSWORD: ' .$this->API_Password,
  'X-PAYPAL-SECURITY-SIGNATURE: ' . $this->API_Signature,
  'X-PAYPAL-SERVICE-VERSION: 1.3.0',
  'X-PAYPAL-APPLICATION-ID: ' . $this->API_AppID
 
  ));

    //if USE_PROXY constant set to TRUE in Constants.php,
    //then only proxy will be enabled.
  //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php
  if($this->USE_PROXY)
   curl_setopt ($ch, CURLOPT_PROXY, $this->PROXY_HOST. ":" . $this->PROXY_PORT);

  // RequestEnvelope fields
  $detailLevel = urlencode("ReturnAll"); // See DetailLevelCode in the WSDL
                                         // for valid enumerations
  $errorLanguage = urlencode("en_US");  // This should be the standard RFC
                                        // 3066 language identification tag,
                                        // e.g., en_US

 // NVPRequest for submitting to server
 $nvpreq = "requestEnvelope.errorLanguage=$errorLanguage&requestEnvelope";
 $nvpreq .= "detailLevel=$detailLevel&$nvpStr";
//echo $nvpreq;exit;
  //setting the nvpreq as POST FIELD to curl
  curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

  //getting response from server
  $response = curl_exec($ch);

  //converting NVPResponse to an Associative Array
  $nvpResArray=$this->deformatNVP($response);

  $nvpReqArray=$this->deformatNVP($nvpreq);

  $_SESSION['nvpReqArray']=$nvpReqArray;

  if (curl_errno($ch))
  {
   // moving to display page to display curl errors
     $_SESSION['curl_error_no']=curl_errno($ch) ;
     $_SESSION['curl_error_msg']=curl_error($ch);

     //Execute the Error handling module to display errors.
  }
  else
  {
    //closing the curl
    curl_close($ch);
  }

  return $nvpResArray;
 }

 /*'----------------------------------------------------------------------------
  Purpose: Redirects to PayPal.com site.
  Inputs:  $cmd is the querystring
  Returns:
 -------------------------------------------------------------------------------
 */
 private function RedirectToPayPal ( $cmd )
 {
  // Redirect to paypal.com here
 // global $Env;

  $payPalURL = "";

  if ($this->Env == "sandbox")
  {
   $payPalURL = "https://www.sandbox.paypal.com/webscr?" . $cmd;
  }
  else
  {
   $payPalURL = "https://www.paypal.com/webscr?" . $cmd;
  }
echo '<script>location.href="'.$payPalURL.'"</script>';
  //header("Location: ".$payPalURL);
 }


 /*'----------------------------------------------------------------------------
   * This private function will take NVPString and convert it to an Associative Array
   * and then will decode the response.
   * It is useful to search for a particular key and display arrays.
   * @nvpstr is NVPString.
   * @nvpArray is Associative Array.
    ----------------------------------------------------------------------------
   */



 private function deformatNVP($nvpstr)
 {
  $intial=0;
  $nvpArray = array();

  while(strlen($nvpstr))
  {
   //postion of Key
   $keypos= strpos($nvpstr,'=');
   //position of value
   $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

   /*getting the Key and Value values and storing in a Associative Array*/
   $keyval=substr($nvpstr,$intial,$keypos);
   $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
   //decoding the respose
   $nvpArray[urldecode($keyval)] =urldecode( $valval);
   $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
      }
  return $nvpArray;
 }

    
}