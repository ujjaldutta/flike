<?php
class My_FeatureDescription_Model_Mysql4_Featuredescription extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("featuredescription/featuredescription", "description_id");
    }
}