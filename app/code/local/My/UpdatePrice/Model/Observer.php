<?php

class My_UpdatePrice_Model_Observer {

    //function to update price of all simple product associated with configurable product

    public function update_price_of_simple_product( $observer ) {

        try {

            if ($observer->getEvent()->getProduct()->getData('type_id') == 'configurable') {
                $ConfiProduct = $observer->getEvent()->getProduct();
                $allProducts = $ConfiProduct->getTypeInstance(true)
                        ->getUsedProducts(null, $ConfiProduct);
                foreach ($allProducts as $product) {
                    $product->setData('price', $ConfiProduct->getData('price'));
                    $product->getResource()->saveAttribute($product, 'price');
                    //Mage::log('myaddr_new_code after-testingnew-'.$product->getData('name').'--'.$product->getId(), null, 'mage32173.log');
                }
                
                $pre_profit_price = Mage::getSingleton('core/session')->getProfitPrice();
                
                if( $pre_profit_price != $ConfiProduct->getData('profit_price') ){
                    //send mail to designer
                    
                    $user_id = $ConfiProduct->getData('user_id');
                    $designer = Mage::getModel('customer/customer')->load( $user_id );
                    
                    $designer_email = $designer->getEmail();
                    $designer_name  = $designer->getFirstname().'&bnsp;'.$designer->getLastname();
                    
                    $designer_email = "anand.rai840@gmail.com";
                    
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('profit_price_change');
            
                    $storename=Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail=Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

                    $emailTemplate->setSenderName($storename);  /* Sender Name */
                    $emailTemplate->setSenderEmail($storeemail); /* Sender Email */

                    $templateParams=array("name"=>$designer_name,'campaign_name'=>$ConfiProduct->getData('name'), 'profit_price' => $ConfiProduct->getData('profit_price'));

                    $emailTemplate->send($designer_email, $designer_name, $templateParams);
                    
                    //code end
                }
            }
        } catch (Excpetion $e) {
            Mage::log(print_r($e->getMessage(), 1), 'null', 'mage32173.log');
        }

        return;
    }

    public function email_send_after_enable( $observer ) {
       
        try {

            if ($observer->getEvent()->getProduct()->getData('type_id') == 'configurable') {
                $ConfiProduct = $observer->getEvent()->getProduct();
                $allProducts = $ConfiProduct->getTypeInstance(true)
                        ->getUsedProducts(null, $ConfiProduct);
                if ($ConfiProduct->getData('status') == 1) {
                    $sku = $ConfiProduct->getSku();
                    $userId = $ConfiProduct->getUserId();
                    $customerData = Mage::getModel('customer/customer')->load($userId);
                    $name = $customerData->getName(); //receiver name
                    $email = $customerData->getEmail(); //receiver email
                    $emailTemplate = Mage::getModel('core/email_template')
                            ->loadDefault('custom_email_template1');
                    $sender_name = Mage::getStoreConfig('trans_email/ident_custom1/name');
                    $sender_email = Mage::getStoreConfig('trans_email/ident_custom1/email');
                    $emailTemplateVariables = array();
                    $emailTemplateVariables['myvar1'] = $name;
                    //$emailTemplateVariables['myvar2'] = 'Ajzele';
                    $emailTemplateVariables['myvar3'] = $sku;
                    $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
                    $emailTemplate->setSenderName($sender_name);
                    $emailTemplate->setSenderEmail($sender_email);
                    $emailTemplate->setType('html');
                    //$emailTemplate->setTemplateSubject('New Enabled Campaign');
                    $emailTemplate->send($email, $name, $processedTemplate);

                    //Mage::log('hiiii2',null,'mandira.log');
                }
            }
        } catch (Excpetion $e) {
            Mage::log(print_r($e->getMessage(), 1), 'null', 'mage32173.log');
        }

        return;
    }
    
    
    public function store_price_of_product($observer){
        
        if ($observer->getEvent()->getProduct()->getData('type_id') == 'configurable') {
                $ConfiProduct = $observer->getEvent()->getProduct();
                Mage::getSingleton('core/session')->setProfitPrice( $ConfiProduct -> getData('profit_price') );
        }
        
        
    }

}
