<?php
class My_Designer_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
//	  $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
//	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
//      $breadcrumbs->addCrumb("home", array(
//                "label" => $this->__("Home Page"),
//                "title" => $this->__("Home Page"),
//                "link"  => Mage::getBaseUrl()
//		   ));
//
//      $breadcrumbs->addCrumb("titlename", array(
//                "label" => $this->__("Titlename"),
//                "title" => $this->__("Titlename")
//		   ));

      $this->renderLayout(); 
	  
    }
    
    public function colorAction() {
      
	  $this->loadLayout();   
		  
          $arg_attribute = 'color';
          $arg_value = 'Red';

          $attr_model = Mage::getModel('catalog/resource_eav_attribute');
          $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
          $attr_id = $attr->getAttributeId();

          $option['attribute_id']    = $attr_id;
          $option['value']['red'][0] = $arg_value;

          $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
         // $setup->addAttributeOption($option);
          
          $this->renderLayout(); 
	  
    }
    
    public function dashboardAction(){
        
       //if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
              //$this->_redirect('');
       //}
          
          
       $this->loadLayout();
       $this->renderLayout();
       
    }
    
    public function beadesignerAction(){
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
              $this->_redirect('');
       }
       
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        
        $user['group_id'] = 2;
        
        $model = Mage::getModel('customer/customer')->load($customer->getId())->addData($user);
        $model->setId($customer->getId())->save();
        Mage::getSingleton('core/session')->addSuccess('You become a designer successfully');
        $this->_redirect('customer/account/profile');
    }
}