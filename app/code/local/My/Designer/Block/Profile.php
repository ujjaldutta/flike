<?php   
class My_Designer_Block_Profile extends Mage_Core_Block_Template{   

    public function getDesignerPublicProfile($entity_id){
        
      $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerData = Mage::getModel('customer/customer')->load($entity_id);
        

        return $customerData;
        
    }
    
   

 //function to get designer gallery
    public function getGallery($entity_id){
        
       
        
        $gallery     = Mage::getModel('designergallery/designergallery')->getCollection()->addFieldToFilter('customer_id',$entity_id);
       //echo $gallery->getSelect()-> __toString();
       //echo $gallery->count();
       
        return $gallery;
        
    }


//function to get designer videos
    public function getVideos($entity_id){
       
        
        $video       = Mage::getModel('designervideo/designervideo')->getCollection()->addFieldToFilter('customer_id',$entity_id);
        
        return $video;
    }
    
    //function to get ongoing campaigns
    public function getOngoingCampaigns($entity_id){
         $helper = Mage::helper('campaign');
       
        $running_option_id       = $helper -> getOptionId('campaign_status','Running');
           
        $products = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)->addFieldToFilter('status', "1")->addFieldToFilter('user_id', $entity_id)->addFieldToFilter('campaign_status', $running_option_id);
        $products->addAttributeToSelect('*');
        return $products;
    }
    
    public function getPreviousCampaigns($entity_id){
       $helper = Mage::helper('campaign');
       
        $running_option_id       = $helper -> getOptionId('campaign_status','Running');
        $products = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)->addFieldToFilter('status', "1")->addFieldToFilter('user_id', $entity_id)->addFieldToFilter('campaign_status', array('neq' => $running_option_id));
        $products->addAttributeToSelect('*');
        return $products;
    }
  
}