<?php
class My_Campaign_Model_Cron{	
    
	public function updateCampaignDuration(){
		
            $helper = Mage::helper('campaign');
       
       $running_option_id       = $helper -> getOptionId('campaign_status','Running');
       $successfull_option_id   = $helper -> getOptionId('campaign_status','Successfull');
       $unsuccessfull_option_id = $helper -> getOptionId('campaign_status','Unsuccessfull');
       
       $products = Mage::getModel('catalog/product')->getCollection()
                    ->addFieldToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)->addFieldToFilter('status', "1")->addFieldToFilter('campaign_status', array('eq' => $running_option_id));
       
       //echo $products -> count();
       
       foreach($products as $product)
        {
            $pro = Mage::getModel('catalog/product')->load($product->getId());
            
            $ordered_quantity  = $pro  -> getQuantityOrdered();
            $sales_target      = $pro  -> getSalesTarget();
            $threshold_limit   = $pro  -> getThresholdLimit();
            
            if($threshold_limit == ""){
                $threshold_limit = $sales_target;
            }
            
            $campaign_duration = $pro -> getCampaignDuration();
            
            //if campaign duration ends and ordered quantity does not reach threshold limit
            if( $campaign_duration == 0 && $ordered_quantity < $threshold_limit ){
                
                $pro->setCampaignStatus($unsuccessfull_option_id);
                $pro->getResource()->saveAttribute($pro, 'campaign_status');
                
                //send mail to administrator
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('campaign_unsuccessfull');

                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

                    $emailTemplate->setSenderName($storename);  /* Sender Name */
                    $emailTemplate->setSenderEmail($storeemail); /* Sender Email */

                    $templateParams=array(
                        'name'          => 'Admin',
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                    );

                    $emailTemplate->send($storeemail, $storename, $templateParams);
                //send mail code end
                    
                    
                //send mail to designer
                   $designer = Mage::getModel('customer/customer') -> load($pro -> getUserId());
                   
                   $designer_name = $designer -> getFirstname().' '. $designer -> getLastname();
                   $designer_mail = $designer -> getEmail();
                   
                   
                   $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                   $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

                   
                   $emailTemplate->setSenderName($storename);  
                   $emailTemplate->setSenderEmail($storeemail);
                   
                   
                   $templateParams=array(
                        'name'          => $designer_name,
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                   );

                   $emailTemplate->send($designer_mail, $designer_name, $templateParams);
                //send mail code end
                   
                //send mail to all customers
                   
                $resource       = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                
                $sql = "SELECT DISTINCT o.customer_id, o.customer_email, o.customer_firstname, o.customer_lastname FROM sales_flat_order_item i
                INNER JOIN sales_flat_order o ON o.entity_id = i.order_id
                WHERE  i.product_id = '".$pro -> getId()."'";
                
                $results = $readConnection->fetchAll($sql);
                
                foreach( $results as $key => $value){
                    
                   //send mail to customers
                    $customer_name = $value['customer_firstname'].' '. $value['customer_lastname'];
                    $customer_mail = $value['customer_email'];
                    
                    
                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */
                    
                    $emailTemplate->setSenderName($storename);  
                    $emailTemplate->setSenderEmail($storeemail);


                    $templateParams=array(
                        'name'          => $customer_name,
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                   );

                    $emailTemplate->send($customer_mail, $customer_name, $templateParams);
                    //send mail code end
                }
                //send mail code end
            }
            
            if($campaign_duration > 0){
                $pro->setCampaignDuration($pro -> getCampaignDuration() - 1);
                $pro->getResource()->saveAttribute($pro, 'campaign_duration');
            }

        }
            
            
            
	} 
        
        
      public function updateCampaignStatus(){
          
          $helper = Mage::helper('campaign');
         
          $running_option_id       = $helper -> getOptionId('campaign_status','Running');
          $successfull_option_id   = $helper -> getOptionId('campaign_status','Successfull');
          $unsuccessfull_option_id = $helper -> getOptionId('campaign_status','Unsuccessfull');
        
        $products = Mage::getModel('catalog/product')->getCollection()
                    ->addFieldToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)->addFieldToFilter('status', "1")->addFieldToFilter('campaign_duration', array('gt' => '0'))->addFieldToFilter('campaign_status', array('eq' => $running_option_id));
        
        foreach($products as $product)
        {
            
             $pro = Mage::getModel('catalog/product')->load($product->getId());
             $ordered_quantity = $pro -> getQuantityOrdered();
             $sales_target     = $pro -> getSalesTarget();
             $threshold_limit  = $pro -> getThresholdLimit();
            
             if($threshold_limit == ""){
                $threshold_limit = $sales_target;
            }
             
             
            $send_threshold = 0;
            //if the quantity ordered reach the sales target
            if( $ordered_quantity >= $sales_target ){
                
                $pro->setCampaignStatus($successfull_option_id);
                $pro->getResource()->saveAttribute($pro, 'campaign_status');
                
                //send mail  to admin
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('sales_target_reach');

                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

                    $emailTemplate->setSenderName($storename);  /* Sender Name */
                    $emailTemplate->setSenderEmail($storeemail); /* Sender Email */

                    $templateParams=array(
                        'name'          => 'Admin',
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                   );

                    $emailTemplate->send($storeemail, $storename, $templateParams);
                //send mail code end
                    
                    
                    
                 //send mail to designer
                    $designer = Mage::getModel('customer/customer') -> load($pro -> getUserId());
                   
                    $designer_name = $designer -> getFirstname().' '. $designer -> getLastname();
                    $designer_mail = $designer -> getEmail();
                    
                    
                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */
                    
                    $emailTemplate->setSenderName($storename);  
                    $emailTemplate->setSenderEmail($storeemail);


                    $templateParams=array(
                        'name'          => $designer_name,
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                   );

                    $emailTemplate->send($designer_mail, $designer_name, $templateParams);
                 //send mail code end
                    
                    
                //send mail to all customers
                   
                $resource       = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                
                $sql = "SELECT DISTINCT o.customer_id, o.customer_email, o.customer_firstname, o.customer_lastname FROM sales_flat_order_item i
                INNER JOIN sales_flat_order o ON o.entity_id = i.order_id
                WHERE  i.product_id = '".$pro -> getId()."'";
                
                $results = $readConnection->fetchAll($sql);
                
                foreach( $results as $key => $value){
                    
                   //send mail to customers
                    $customer_name = $value['customer_firstname'].' '. $value['customer_lastname'];
                    $customer_mail = $value['customer_email'];
                    
                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */
                    
                    
                    $emailTemplate->setSenderName($storename);  
                    $emailTemplate->setSenderEmail($storeemail);


                    $templateParams=array(
                        'name'          => $customer_name,
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                   );

                    $emailTemplate->send($customer_mail, $customer_name, $templateParams);
                    //send mail code end
                }
                //send mail code end
               
            }else{
                $send_threshold = 1;
            }
            
            //if the quantity ordered does not reach sales target but reached the threshold limit set by admin
            if( $ordered_quantity >= $threshold_limit && $send_threshold == 1){
                
                //send mail code to admin
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('threshold_limit_reach');

                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

                    $emailTemplate->setSenderName($storename);  /* Sender Name */
                    $emailTemplate->setSenderEmail($storeemail); /* Sender Email */

                   $templateParams=array(
                        'name'          => 'Admin',
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                   );

                    $emailTemplate->send($storeemail, $storename, $templateParams);
                //end
                    
                
                    
                      //send mail to designer
                    $designer = Mage::getModel('customer/customer') -> load($pro -> getUserId());
                   
                    $designer_name = $designer -> getFirstname().' '. $designer -> getLastname();
                    $designer_mail = $designer -> getEmail();
                    
                    
                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */
                    
                    $emailTemplate->setSenderName($storename);  
                    $emailTemplate->setSenderEmail($storeemail);


                    $templateParams=array(
                        'name'          => $designer_name,
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                   );

                    $emailTemplate->send($designer_mail, $designer_name, $templateParams);
                 //send mail code end
                    
                    
                //send mail to all customers
                   
                $resource       = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                
                $sql = "SELECT DISTINCT o.customer_id, o.customer_email, o.customer_firstname, o.customer_lastname FROM sales_flat_order_item i
                INNER JOIN sales_flat_order o ON o.entity_id = i.order_id
                WHERE  i.product_id = '".$pro -> getId()."'";
                
                $results = $readConnection->fetchAll($sql);
                
                foreach( $results as $key => $value){
                    
                   //send mail to customers
                    $customer_name = $value['customer_firstname'].' '. $value['customer_lastname'];
                    $customer_mail = $value['customer_email'];
                    
                    
                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */
                    
                    $emailTemplate->setSenderName($storename);  
                    $emailTemplate->setSenderEmail($storeemail);


                    $templateParams=array(
                        'name'          => $customer_name,
                        'campaign_name' => $pro -> getName(),
                        'sku'           => $pro -> getSku()
                   );

                    $emailTemplate->send($customer_mail, $customer_name, $templateParams);
                    //send mail code end
                }
                //send mail code end
                    
                    
                    
                    
            }
            
            
        }
        
    }
        
        
}