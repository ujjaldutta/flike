<?php
class My_Campaign_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getOptionId($attribute_code,$label)
    {
        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;
        $attribute_code = $attribute_model->getIdByCode('catalog_product', $attribute_code);
        $attribute = $attribute_model->load($attribute_code);

        $attribute_table = $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        foreach($options as $option)
        {
        if ($option['label'] == $label)
        {
        $optionId=$option['value'];
        break;
        }
        }
        return $optionId;
    }
}
	 