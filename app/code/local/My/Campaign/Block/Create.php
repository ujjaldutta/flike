<?php   
class My_Campaign_Block_Create extends Mage_Core_Block_Template{   

    public function getCategory(){
       
        $categories = Mage::getModel('catalog/category')->getCollection()->setStoreId(1)
                ->addFieldToFilter('is_active', 1)->addAttributeToSelect('*')->addAttributeToFilter('level', 2);
       
        return $categories;
        
    }
    
   
}