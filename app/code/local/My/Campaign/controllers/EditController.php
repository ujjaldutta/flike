<?php
class My_Campaign_EditController extends Mage_Core_Controller_Front_Action{
    
    //function to get subcategory
    public function indexAction(){
        
        $this->loadLayout();
         $this->getLayout()->getBlock("head")->setTitle($this->__("Edit campaign information"));
        $this->renderLayout();
    }
    
    
    public function editPostAction(){
       
      $id             =  $this->getRequest()->getPost("product_id");
      $description    =  $this->getRequest()->getPost("product_description");
      $product_image  =  $this->getRequest()->getPost("product_image");
      
      
      $configProduct         =  Mage::getModel('catalog/product')->loadByAttribute('sku', $id);
      //$product_id = $configProduct->getIdBySku ( $id);
      //$configProduct->load ( $product_id );
      //$product->getId ();

      //$configProduct -> setDescription($description);
     // $configProduct -> setShortDescription($description);
      
     // $configProduct -> save();
      
     /* $mediaGalleryBackendModel = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'media_gallery')->getBackend();
            $arrayToMassAdd     = array();

            foreach ($product_image as $val) {
                
                $uploaded = explode("@#@", $val);
                $key      = $uploaded[0];
                $importData = array("media_gallery" => "/" . $key, "image" => "/" . $key,
                    "small_image" => "/" . $key, "thumbnail" => "/" . $key
                );
                foreach ($configProduct->getMediaAttributes() as $mediaAttributeCode => $mediaAttribute) {
                    if (isset($importData[$mediaAttributeCode])) {
                        $file = trim($importData[$mediaAttributeCode]);
                        if (!empty($file) && !$mediaGalleryBackendModel->getImage($configProduct, $file)) {
                            $arrayToMassAdd[] = array('file' => trim($file), 'mediaAttribute' => $mediaAttributeCode);
                        }
                    }
                }
            }


            $addedFilesCorrespondence = $mediaGalleryBackendModel->addImagesWithDifferentMediaAttributes(
                    $configProduct, $arrayToMassAdd, Mage::getBaseDir('media') . DS . 'import', true, false
            );
            $configProduct -> save();*/
       
       
       
       $attributes = $configProduct->getTypeInstance ()->getSetAttributes (); 
       $gallery = $attributes['media_gallery'];
       $gallery->getBackend () -> afterLoad($configProduct);
      
      if(is_array($product_image) && count($product_image) > 0){
        foreach ($product_image as $val) {

                  $uploaded = explode("@#@", $val);
                  $gallery_img      = $uploaded[0];
                  $ourProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $id);
                  $ourProduct->setMediaGallery ( array ('images' => array (), 'values' => array () ) );
                  if (file_exists ( Mage::getBaseDir ( 'media' ) . DS . 'import' .DS. $gallery_img )) {
                      $ourProduct->addImageToMediaGallery ( Mage::getBaseDir ( 'media' ) . DS . 'import' . DS.$gallery_img, array ("thumbnail", "small_image", "image" ), false, false );
                  }

        }
        $ourProduct -> save();
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
      
     
        
      }
       
         $configProduct -> setDescription($description);
      $configProduct -> setShortDescription($description);
       $configProduct->save();
      Mage::getSingleton('core/session')->addSuccess('Campaign Updated Successfully');
      $this->_redirect('campaign/edit/index', array('id' => $id));
    }
    
}