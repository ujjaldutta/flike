<?php
class My_Campaign_AjaxController extends Mage_Core_Controller_Front_Action{
    
    //function to get subcategory
    public function getSubcategoryAction(){
        
        $category_id    = Mage::app()->getRequest()->getParam('category_id'); 
        $sub_categories = Mage::getModel('catalog/category')->getCategories($category_id);
        $output = "";
        $output.=  '<select  id="subcategory_id"  name="subcategory_id" class="subcategory_id validate-select">';
        $output.=  '<option value="">Select category</option>';
                            
        foreach ($sub_categories as $subcat) 
        {
                 
         
                 $output.=  '<option value="'.$subcat->getId().'">'.$subcat->getName().'</option>';
                 
        } 
                                    
        $output.= '</select>';

        echo $output;
        exit;
    }
    
     //function to get type
    public function getTypeAction(){
        
        $subcategory_id = Mage::app()->getRequest()->getParam('subcategory_id'); 
        $type           = Mage::getModel('catalog/category')->getCategories($subcategory_id);
        $output = "";
        $output.=  '<select  id="product_type"  name="product_type" class="validate-select">';
        $output.=  '<option value="">Select type</option>';
                            
        foreach ($type as $value) 
        {
                 
         
                 $output.=  '<option value="'.$value->getId().'">'.$value->getName().'</option>';
                 
        } 
                                    
        $output.= '</select>';

        echo $output;
        exit;
    }
    
    
    //function to upload raw design files
    public function AddRawDesignAction(){
        if (isset($_FILES)){
				
                if ($_FILES['formData']['name']) {

                        $path = Mage::getBaseDir('media') . DS . 'product_rawdesign' . DS;
                        $uploader = new Varien_File_Uploader('formData');
                        $uploader->setAllowedExtensions(array('jpg','jpeg','png', 'JPG', 'JPEG', 'PNG'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $destFile = $path.$_FILES['formData']['name'];
                        $filename = $uploader->getNewFileName($destFile);
                        $result = $uploader->save($path, $filename);
                         
                        $post_data['path'] = 'product_rawdesign/'.$result['file'];
                        $post_data['uploaded_raw_design'] = $result['file'];
                        $post_data['product_id'] = "";
                        $post_data['name'] = $_FILES['formData']['name'];
                        
                        $arr = Mage::getSingleton('core/session')->getRawDesign();
                        $arr[$filename] = json_encode($post_data);
                        Mage::getSingleton('core/session')->setRawDesign($arr);
                        echo json_encode($post_data);
                        exit;
                }
         }
    }
    
    //function to add product images
    public function AddProductImageAction(){
        
          if (isset($_FILES)){
				
                if ($_FILES['formData']['name']) {
                        
                        $path = Mage::getBaseDir('media') . DS . 'import' . DS;
                        $uploader = new Varien_File_Uploader('formData');
                        $uploader->setAllowedExtensions(array('jpg','jpeg', 'png', 'JPG', 'JPEG', 'PNG'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $destFile = $path.$_FILES['formData']['name'];
                        $filename = $uploader->getNewFileName($destFile);
                        $result = $uploader->save($path, $filename);
                        //print_r($result);
                        //exit;
                        
                        $post_data['uploaded_image_name'] = $result['file'];
                        //echo $post_data['uploaded_image_name'];exit;
                        $post_data['name'] = $_FILES['formData']['name'];
                        $arr = Mage::getSingleton('core/session')->getProductImage();
                        $arr[$filename] = json_encode($post_data);
                        Mage::getSingleton('core/session')->setProductImage($arr);
                        echo json_encode($post_data);
                        exit;
                        
                }
            }
        
    }
    
    //function to add product images
    public function editProductImageAction(){
        
          if (isset($_FILES)){
				
                if ($_FILES['formData']['name']) {
                        
                        $path = Mage::getBaseDir('media') . DS . 'import' . DS;
                        $uploader = new Varien_File_Uploader('formData');
                        $uploader->setAllowedExtensions(array('jpg','jpeg', 'png', 'JPG', 'JPEG', 'PNG'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $destFile = $path.$_FILES['formData']['name'];
                        $filename = $uploader->getNewFileName($destFile);
                        $result = $uploader->save($path, $filename);
                        //print_r($result);
                        //exit;
                        
                        $post_data['uploaded_image_name'] = $result['file'];
                        //echo $post_data['uploaded_image_name'];exit;
                        $post_data['name'] = $_FILES['formData']['name'];
                        //$arr = Mage::getSingleton('core/session')->getProductImage();
                        //$arr[$filename] = json_encode($post_data);
                        //Mage::getSingleton('core/session')->setProductImage($arr);
                        echo json_encode($post_data);
                        exit;
                        
                }
            }
        
    }
    
    
    //function to delete raw design
    public function deleterawdesignAction(){
        $raw_design = Mage::app()->getRequest()->getParam('raw_design'); 
          if($raw_design){
              
                $io = new Varien_Io_File();
                $io->rm(Mage::getBaseDir('media').DS.'product_rawdesign'.DS.$raw_design);
                $raw = Mage::getSingleton('core/session')->getRawDesign();
                unset($raw[$raw_design]);
                Mage::getSingleton('core/session')->setRawDesign($raw);
                exit;
           
          }
        
    }
    
    //function to delete raw design
    public function deleteproimageAction(){
        $pro_image = Mage::app()->getRequest()->getParam('pro_image'); 
          if($pro_image){
              
                $io = new Varien_Io_File();
                $io->rm(Mage::getBaseDir('media').DS.'import'.DS.$pro_image);
                $pro = Mage::getSingleton('core/session')->getProductImage();
                unset($pro[$pro_image]);
                Mage::getSingleton('core/session')->setProductImage($pro);
                exit;
           
          }
        
    }
    
    
     //function to delete raw design
    public function deleteeditedproimageAction(){
        $pro_image = Mage::app()->getRequest()->getParam('pro_image'); 
          if($pro_image){
              
                $io = new Varien_Io_File();
                $io->rm(Mage::getBaseDir('media').DS.'import'.DS.$pro_image);
                $pro = Mage::getSingleton('core/session')->getProductImage();
                unset($pro[$pro_image]);
                Mage::getSingleton('core/session')->setProductImage($pro);
                exit;
           
          }
        
    }
}