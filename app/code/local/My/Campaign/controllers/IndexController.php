<?php
class My_Campaign_IndexController extends Mage_Core_Controller_Front_Action{
    
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Titlename"),
                "title" => $this->__("Titlename")
		   ));

      $this->renderLayout(); 
	  
    }
    
    public function update_campaign_durationAction(){
        
       $helper = Mage::helper('campaign');
       
       $running_option_id       = $helper -> getOptionId('campaign_status','Running');
       $successfull_option_id   = $helper -> getOptionId('campaign_status','Successfull');
       $unsuccessfull_option_id = $helper -> getOptionId('campaign_status','Unsuccessfull');
       
       $products = Mage::getModel('catalog/product')->getCollection()
                    ->addFieldToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)->addFieldToFilter('status', "1")->addFieldToFilter('campaign_status', array('eq' => $running_option_id));
       
       //echo $products -> count();
       
       foreach($products as $product)
        {
            $pro = Mage::getModel('catalog/product')->load($product->getId());
            
            $ordered_quantity  = $pro  -> getQuantityOrdered();
            $sales_target      = $pro  -> getSalesTarget();
            $threshold_limit   = $pro  -> getThresholdLimit();
            
            if($threshold_limit == ""){
                $threshold_limit = $sales_target;
            }
            
            $campaign_duration = $pro -> getCampaignDuration();
            
            //if campaign duration ends and ordered quantity does not reach threshold limit
            if( $campaign_duration == 0 && $ordered_quantity < $threshold_limit ){
                
                $pro->setCampaignStatus($unsuccessfull_option_id);
                $pro->getResource()->saveAttribute($pro, 'campaign_status');
                
                //send mail
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('campaign_unsuccessfull');

                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

                    $emailTemplate->setSenderName($storename);  /* Sender Name */
                    $emailTemplate->setSenderEmail($storeemail); /* Sender Email */

                    $templateParams=array();

                    $emailTemplate->send($storeemail, $storename, $templateParams);
                //send mail code end
            }
            
            if($campaign_duration > 0){
                $pro->setCampaignDuration($pro -> getCampaignDuration() - 1);
                $pro->getResource()->saveAttribute($pro, 'campaign_duration');
            }

        }
    }
    
    public function update_campaign_statusAction(){
        
          $helper = Mage::helper('campaign');
         
          $running_option_id       = $helper -> getOptionId('campaign_status','Running');
          $successfull_option_id   = $helper -> getOptionId('campaign_status','Successfull');
          $unsuccessfull_option_id = $helper -> getOptionId('campaign_status','Unsuccessfull');
        
        $products = Mage::getModel('catalog/product')->getCollection()
                    ->addFieldToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)->addFieldToFilter('status', "1")->addFieldToFilter('campaign_duration', array('gt' => '0'))->addFieldToFilter('campaign_status', array('eq' => $running_option_id));
        
        foreach($products as $product)
        {
            
             $pro = Mage::getModel('catalog/product')->load($product->getId());
             $ordered_quantity = $pro -> getQuantityOrdered();
             $sales_target      = $pro -> getSalesTarget();
             $threshold_limit  = $pro -> getThresholdLimit();
            
             if($threshold_limit == ""){
                $threshold_limit = $sales_target;
            }
             
             
            $send_threshold = 0;
            //if the quantity ordered reach the sales target
            if( $ordered_quantity >= $sales_target ){
                
                $pro->setCampaignStatus($successfull_option_id);
                $pro->getResource()->saveAttribute($pro, 'campaign_status');
                
                //send mail
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('sales_target_reach');

                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

                    $emailTemplate->setSenderName($storename);  /* Sender Name */
                    $emailTemplate->setSenderEmail($storeemail); /* Sender Email */

                    $templateParams=array();

                    $emailTemplate->send($storeemail, $storename, $templateParams);
                //send mail code end
               
            }else{
                $send_threshold = 1;
            }
            
            //if the quantity ordered does not reach sales target but reached the threshold limit set by admin
            if( $ordered_quantity >= $threshold_limit && $send_threshold == 1){
                
                //send mail code
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('threshold_limit_reach');

                    $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

                    $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

                    $emailTemplate->setSenderName($storename);  /* Sender Name */
                    $emailTemplate->setSenderEmail($storeemail); /* Sender Email */

                    $templateParams=array();

                    $emailTemplate->send($storeemail, $storename, $templateParams);
                //end
            }
            
            
        }
        
    }
    
    
    public function testAction(){
        
        $order_id = "100000104";
      // $order = Mage::getModel("sales/order")->loadByIncrementId($order_id);
       
        
        $_order = Mage::getModel("sales/order")->loadByIncrementId($order_id);
        
        $items = $_order->getAllItems();
        foreach ($items as $item){
        $base_grand_total = $_order->getBaseGrandTotal();

        $base_subtotal = $_order->getBaseSubtotal();
        $base_tva      = $_order->getBaseTaxAmount();

        $grand_total = $_order->getGrandTotal();

        $subtotal = $_order->getSubtotal();
        $tva      = $_order->getTaxAmount();


        $base_subtotal_incl_tax = $_order->getBaseSubtotalInclTax();

        $subtotal_incl_tax = $_order->getSubtotalInclTax();

        $total_item_count  = $_order->getTotalItemCount();

        
        
       //if($item->getSku()=='flike_55c934e859ab7'){
            $item_price = $item->getPrice();
            $item_tva   = $item->getTaxAmount();
           // $item->delete();
           /* $_order->setBaseGrandTotal($base_grand_total-$item_price-$item_tva);

            $_order->setBaseSubtotal($base_subtotal-$item_price);

            $_order->setBaseTaxAmount($base_tva-$item_tva);

            $_order->setGrandTotal($grand_total-$item_price-$item_tva);

            $_order->setSubtotal($subtotal-$item_price);

            $_order->setTaxAmount($tva-$item_tva);


            $_order->setBaseSubtotalInclTax($base_subtotal_incl_tax-$item_price);

            $_order->setSubtotalInclTax($subtotal_incl_tax-$item_price);*/

            //$_order->setTotalItemCount($total_item_count+1);

           // $_order->save(); 
            //echo "updated";
        //}

        }
       
       
       
       
    }
    
    
    public function testmailAction(){
         $emailTemplate = Mage::getModel('core/email_template')->loadDefault('custom_email_after_order_place');

            $storename  = Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */

            $storeemail = Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */

            $emailTemplate->setSenderName("Flike");  /* Sender Name */
            $emailTemplate->setSenderEmail("anand.rai840@gmail.com"); /* Sender Email */

            $templateParams=array();

            $emailTemplate->send('ruma.bose86@gmail.com', $storename, $templateParams);
    }
    
    public function testsqlAction(){
        $resource       = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $sql = "SELECT DISTINCT o.customer_id, o.customer_email, o.customer_firstname, o.customer_lastname FROM sales_flat_order_item i
                INNER JOIN sales_flat_order o ON o.entity_id = i.order_id
                WHERE  i.product_id = '321'";

        $results = $readConnection->fetchAll($sql);
        
        
        print '<pre>';
        print_r($results);
    }
    
    public function getcatAction(){
        $category = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('name', 'Men');   
        $cat= $category->getData();
         $categoryid = $cat[0][entity_id];
        
       echo $parentId = Mage::getModel('catalog/category')->load($categoryid)->getParentId();
    }

}