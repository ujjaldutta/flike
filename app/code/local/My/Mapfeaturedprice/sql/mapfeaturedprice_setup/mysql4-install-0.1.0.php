<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table productfeature_price(id int not null auto_increment, 
type_id int  NOT NULL,
feature varchar(255) NOT NULL,
price varchar(255) NOT NULL,
primary key(id));
SQLTEXT;

$sql2=<<<SQLTEXT
create table category_price(id int not null auto_increment, 
type_id int  NOT NULL,
price varchar(255) NOT NULL,
primary key(id));
SQLTEXT;
$installer->run($sql);
$installer->run($sql2);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 