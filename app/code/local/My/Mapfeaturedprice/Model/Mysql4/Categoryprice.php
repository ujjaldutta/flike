<?php
class My_Mapfeaturedprice_Model_Mysql4_Categoryprice extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("mapfeaturedprice/mapfeaturedprice", "price_id");
        $this->_init('mapfeaturedprice/categoryprice', 'id'); 
    }
}