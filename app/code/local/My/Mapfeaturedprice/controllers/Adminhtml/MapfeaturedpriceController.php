<?php

class My_Mapfeaturedprice_Adminhtml_MapfeaturedpriceController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("mapfeaturedprice/mapfeaturedprice")->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapfeaturedprice  Manager"),Mage::helper("adminhtml")->__("Mapfeaturedprice Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Mapfeaturedprice"));
			    $this->_title($this->__("Manager Mapfeaturedprice"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
                    $id     = $this->getRequest()->getPost('id');
                    $price  = $this->getRequest()->getPost('price');
                    $feature_name  = $this->getRequest()->getPost('feature_name');
                    
                    if($price == "" || $feature_name == ""){
                        
                        Mage::getSingleton('core/session')->addError('Please fill all the fields'); 
                        $this->_redirect('admin_mapfeaturedprice/adminhtml_mapfeaturedprice/index' ,array('action' => 'edit', 'id' => $id));
                            
                    }else{
                        $exist = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->getCollection()->addFieldToFilter('type_id', $product_type)->addFieldToFilter('feature', $feature_name)->addFieldToFilter('id', array('neq'=>$id))->addFieldToSelect('type_id');

                        if($exist -> count() != 0){
                            Mage::getSingleton('core/session')->addError('Feature Already Added for this Type'); 
                            $this->_redirect('admin_mapfeaturedprice/adminhtml_mapfeaturedprice/index');
                        }else{
                        $data['price']   = $price;
                        $data['feature']   = $feature_name;
                        $model = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->load($id)->addData($data);

                        $model->setId($id)->save();
                        Mage::getSingleton('core/session')->addSuccess('Record Updated Successfully'); 
                        $this->_redirect('admin_mapfeaturedprice/adminhtml_mapfeaturedprice/index');   
                        }
                    }
		}

		public function newAction()
		{

		
		}
		public function saveAction()
		{
                    $product_type  = $this->getRequest()->getPost('product_type');
                    $feature_name  = $this->getRequest()->getPost('feature_name');
                    $price         = $this->getRequest()->getPost('price');
                    if($product_type == "" || $feature_name == "" || $price == ""){
                        
                        Mage::getSingleton('core/session')->addError('Please fill all the fields'); 
                        $this->_redirect('admin_mapfeaturedprice/adminhtml_mapfeaturedprice/index');
                        
                    }else{
                        $exist = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->getCollection()->addFieldToFilter('type_id', $product_type)->addFieldToFilter('feature', $feature_name)->addFieldToSelect('type_id');

                        if($exist -> count() != 0){
                            Mage::getSingleton('core/session')->addError('Feature Already Added for this Type'); 
                            $this->_redirect('admin_mapfeaturedprice/adminhtml_mapfeaturedprice/index');
                        }else{
                        $data['type_id'] = $product_type;
                        $data['price']   = $price;
                        $data['feature'] = $feature_name;
                        $model = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->setData($data);

                        $model->save();
                        Mage::getSingleton('core/session')->addSuccess('Record Added Successfully'); 
                        $this->_redirect('admin_mapfeaturedprice/adminhtml_mapfeaturedprice/index');
                        }
                    }
			
		}



		public function deleteAction()
		{
			$id  = $this->getRequest()->getParam('id');
                        if($id){
                            $model = Mage::getModel('mapfeaturedprice/mapfeaturedprice');
                            try {
                                $model->setId($id)->delete();
                                Mage::getSingleton('core/session')->addSuccess('Record Deleted Successfully'); 
                                $this->_redirect('admin_mapfeaturedprice/adminhtml_mapfeaturedprice/index');

                            } catch (Exception $e){
                                echo $e->getMessage(); 
                            }
                        }
		}

}
