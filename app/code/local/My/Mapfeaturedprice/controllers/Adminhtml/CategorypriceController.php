<?php

class My_Mapfeaturedprice_Adminhtml_CategorypriceController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("mapfeaturedprice/mapfeaturedprice")->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapfeaturedprice  Manager"),Mage::helper("adminhtml")->__("Mapfeaturedprice Manager"));
				return $this;
		}
		public function indexAction() 
		{
				
				
			    $this->_title($this->__("Category Price"));
			    $this->_title($this->__("Manager Categoryprice"));

			    $this->_initAction();
			    $this->renderLayout();
		}
		public function editAction()
		{
                    $id     = $this->getRequest()->getPost('id');
                    $price  = $this->getRequest()->getPost('price');
                    
                     if( $price == "" ){
                         
                        Mage::getSingleton('core/session')->addError('Please fill the price field'); 
                        $this->_redirect('admin_mapfeaturedprice/adminhtml_categoryprice/index', array('action' => 'edit', 'id' => $id));
                        
                    }else{
                        $data['price']   = $price;
                        $model = Mage::getModel('mapfeaturedprice/categoryprice')->load($id)->addData($data);

                        $model->setId($id)->save();
                        Mage::getSingleton('core/session')->addSuccess('Record Updated Successfully'); 
                        $this->_redirect('admin_mapfeaturedprice/adminhtml_categoryprice/index');
                    }
		}

		public function newAction()
		{
		}
		public function saveAction()
		{
                    
                    $product_type  = $this->getRequest()->getPost('product_type');
                    $price         = $this->getRequest()->getPost('price');
                    
                    if($product_type == "" || $price == ""){
                        Mage::getSingleton('core/session')->addError('Please select Type and fill the price field'); 
                        $this->_redirect('admin_mapfeaturedprice/adminhtml_categoryprice/index');
                    }else{
                        $exist = Mage::getModel('mapfeaturedprice/categoryprice')->getCollection()->addFieldToFilter('type_id', $product_type)->addFieldToSelect('type_id');
                        if($exist -> count() > 0){
                            Mage::getSingleton('core/session')->addError('Already Price Added for this Type'); 
                            $this->_redirect('admin_mapfeaturedprice/adminhtml_categoryprice/index');
                        }else{
                        $data['type_id'] = $product_type;
                        $data['price']   = $price;
                        $model = Mage::getModel('mapfeaturedprice/categoryprice')->setData($data);

                        $model->save();
                        Mage::getSingleton('core/session')->addSuccess('Record Added Successfully'); 
                        $this->_redirect('admin_mapfeaturedprice/adminhtml_categoryprice/index');
                        }
                    }
		}



		public function deleteAction()
		{
			$id  = $this->getRequest()->getParam('id');
                        if($id){
                            $model = Mage::getModel('mapfeaturedprice/categoryprice');
                            try {
                                $model->setId($id)->delete();
                                Mage::getSingleton('core/session')->addSuccess('Record Deleted Successfully'); 
                                $this->_redirect('admin_mapfeaturedprice/adminhtml_categoryprice/index');

                            } catch (Exception $e){
                                echo $e->getMessage(); 
                            }
                        }
		}

		
		public function massRemoveAction()
		{
			
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			
		}
		
		
}
