<?php

class My_Mapfeaturedprice_Adminhtml_MapfeaturedpriceController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("mapfeaturedprice/mapfeaturedprice")->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapfeaturedprice  Manager"),Mage::helper("adminhtml")->__("Mapfeaturedprice Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Mapfeaturedprice"));
			    $this->_title($this->__("Manager Mapfeaturedprice"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Mapfeaturedprice"));
				$this->_title($this->__("Mapfeaturedprice"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("mapfeaturedprice/mapfeaturedprice")->load($id);
				if ($model->getId()) {
					Mage::register("mapfeaturedprice_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("mapfeaturedprice/mapfeaturedprice");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapfeaturedprice Manager"), Mage::helper("adminhtml")->__("Mapfeaturedprice Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapfeaturedprice Description"), Mage::helper("adminhtml")->__("Mapfeaturedprice Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("mapfeaturedprice/adminhtml_mapfeaturedprice_edit"))->_addLeft($this->getLayout()->createBlock("mapfeaturedprice/adminhtml_mapfeaturedprice_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("mapfeaturedprice")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Mapfeaturedprice"));
		$this->_title($this->__("Mapfeaturedprice"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("mapfeaturedprice/mapfeaturedprice")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("mapfeaturedprice_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("mapfeaturedprice/mapfeaturedprice");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapfeaturedprice Manager"), Mage::helper("adminhtml")->__("Mapfeaturedprice Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Mapfeaturedprice Description"), Mage::helper("adminhtml")->__("Mapfeaturedprice Description"));


		$this->_addContent($this->getLayout()->createBlock("mapfeaturedprice/adminhtml_mapfeaturedprice_edit"))->_addLeft($this->getLayout()->createBlock("mapfeaturedprice/adminhtml_mapfeaturedprice_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("mapfeaturedprice/mapfeaturedprice")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Mapfeaturedprice was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setMapfeaturedpriceData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setMapfeaturedpriceData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("mapfeaturedprice/mapfeaturedprice");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('price_ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("mapfeaturedprice/mapfeaturedprice");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'mapfeaturedprice.csv';
			$grid       = $this->getLayout()->createBlock('mapfeaturedprice/adminhtml_mapfeaturedprice_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'mapfeaturedprice.xml';
			$grid       = $this->getLayout()->createBlock('mapfeaturedprice/adminhtml_mapfeaturedprice_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
		
		
}
