<?php
class My_Mapfeaturedprice_Block_Adminhtml_Mapfeaturedprice_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("mapfeaturedprice_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("mapfeaturedprice")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("mapfeaturedprice")->__("Item Information"),
				"title" => Mage::helper("mapfeaturedprice")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("mapfeaturedprice/adminhtml_mapfeaturedprice_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
