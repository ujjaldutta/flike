<?php

class My_Mapfeaturedprice_Block_Adminhtml_Mapfeaturedprice_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("mapfeaturedpriceGrid");
				$this->setDefaultSort("price_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("mapfeaturedprice/mapfeaturedprice")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("price_id", array(
				"header" => Mage::helper("mapfeaturedprice")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "price_id",
				));
                
						$this->addColumn('feature_id', array(
						'header' => Mage::helper('mapfeaturedprice')->__('Feature Name'),
						'index' => 'feature_id',
						'type' => 'options',
						'options'=>My_Mapfeaturedprice_Block_Adminhtml_Mapfeaturedprice_Grid::getOptionArray0(),				
						));
						
				$this->addColumn("price", array(
				"header" => Mage::helper("mapfeaturedprice")->__("Price"),
				"index" => "price",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('price_id');
			$this->getMassactionBlock()->setFormFieldName('price_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_mapfeaturedprice', array(
					 'label'=> Mage::helper('mapfeaturedprice')->__('Remove Mapfeaturedprice'),
					 'url'  => $this->getUrl('*/adminhtml_mapfeaturedprice/massRemove'),
					 'confirm' => Mage::helper('mapfeaturedprice')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray0()
		{
                        $data_array=array(); 
                        $attribute_code = "product_features"; 
                        $attribute_details = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $attribute_code);
                        $options = $attribute_details->getSource()->getAllOptions(false); 
                        foreach($options as $option){
                            $data_array[$option["value"]] = $option["label"];
                        }
                        
			//$data_array[0]='id1';
			//$data_array[1]='id2';
                        return($data_array);
		}
		static public function getValueArray0()
		{
            $data_array=array();
			foreach(My_Mapfeaturedprice_Block_Adminhtml_Mapfeaturedprice_Grid::getOptionArray0() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}