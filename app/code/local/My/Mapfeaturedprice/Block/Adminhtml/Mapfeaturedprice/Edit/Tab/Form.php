<?php
class My_Mapfeaturedprice_Block_Adminhtml_Mapfeaturedprice_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("mapfeaturedprice_form", array("legend"=>Mage::helper("mapfeaturedprice")->__("Item information")));

								
						 $fieldset->addField('feature_id', 'select', array(
						'label'     => Mage::helper('mapfeaturedprice')->__('Feature Name'),
						'values'   => My_Mapfeaturedprice_Block_Adminhtml_Mapfeaturedprice_Grid::getValueArray0(),
						'name' => 'feature_id',
						));
						$fieldset->addField("price", "text", array(
						"label" => Mage::helper("mapfeaturedprice")->__("Price"),
						"name" => "price",
                                                "class" => "required-entry",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getMapfeaturedpriceData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getMapfeaturedpriceData());
					Mage::getSingleton("adminhtml/session")->setMapfeaturedpriceData(null);
				} 
				elseif(Mage::registry("mapfeaturedprice_data")) {
				    $form->setValues(Mage::registry("mapfeaturedprice_data")->getData());
				}
				return parent::_prepareForm();
		}
}
