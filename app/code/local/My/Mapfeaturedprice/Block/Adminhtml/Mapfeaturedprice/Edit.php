<?php
	
class My_Mapfeaturedprice_Block_Adminhtml_Mapfeaturedprice_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "price_id";
				$this->_blockGroup = "mapfeaturedprice";
				$this->_controller = "adminhtml_mapfeaturedprice";
				$this->_updateButton("save", "label", Mage::helper("mapfeaturedprice")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("mapfeaturedprice")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("mapfeaturedprice")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("mapfeaturedprice_data") && Mage::registry("mapfeaturedprice_data")->getId() ){

				    return Mage::helper("mapfeaturedprice")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("mapfeaturedprice_data")->getId()));

				} 
				else{

				     return Mage::helper("mapfeaturedprice")->__("Add Item");

				}
		}
}