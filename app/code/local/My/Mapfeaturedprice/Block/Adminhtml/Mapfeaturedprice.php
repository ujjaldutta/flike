<?php


class My_Mapfeaturedprice_Block_Adminhtml_Mapfeaturedprice extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_mapfeaturedprice";
	$this->_blockGroup = "mapfeaturedprice";
	$this->_headerText = Mage::helper("mapfeaturedprice")->__("Mapfeaturedprice Manager");
	$this->_addButtonLabel = Mage::helper("mapfeaturedprice")->__("Add New Item");
	parent::__construct();
	
	}
        
        public function getCategory(){
            
            $categories = Mage::getModel('catalog/category')->getCollection()->setStoreId(1)
                ->addFieldToFilter('is_active', 1)->addAttributeToSelect('*')->addAttributeToFilter('level', 2);
       
        return $categories;
        }
        
        public function getAllTypePrice(){
            
            $types = Mage::getModel('mapfeaturedprice/categoryprice')->getCollection();
            return $types;
            
        }
        
        public function getAllFeaturePrice(){
             $p = $this->getRequest()->getPost('product_type');
             $f = $this->getRequest()->getPost('feature_name');
            if($p != "" && $f == ""){
               $feature = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->getCollection()->addFieldToFilter('type_id', $p); 
            }
            if($f!= "" && $p == ""){
                $feature = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->getCollection()->addFieldToFilter("feature",array('like'=>"%".$f."%"));
            }
            
            if($p != "" && $f != ""){
                $feature = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->getCollection()->addFieldToFilter("feature",array('like'=>"%".$f."%"))->addFieldToFilter('type_id', $p);
               // echo $feature->getSelect()->__toString();
                //exit;
            }
            
            if($p == "" && $f == ""){
                $feature = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->getCollection();
            }
            //$feature = Mage::getModel('mapfeaturedprice/mapfeaturedprice')->getCollection()->addFieldToFilter(array('attribute' => 'feature_name', 'like' => "%'".$f."'%"))->addFieldToFilter('type_id', $p);
            return $feature;
        }

}