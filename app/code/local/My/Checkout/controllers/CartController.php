<?php
/**
 * Customer front controller
 *
 * @category    Customer
 * @package     My Customer
 * @author      Anand Kumar Rai 
 */

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';

class My_Checkout_CartController extends Mage_Checkout_CartController
{
    /**
     * Add product to shopping cart action
     *
     * @return Mage_Core_Controller_Varien_Action
     * @throws Exception
     */
    public function addAction()
    {
        
       
      
        if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart   = $this->_getCart();
        
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if($quote->getItemsCount()>=1){
            $this->_getSession()->addError('You can only buy one product at a time.');
            $this->_redirectReferer();
            return;
        }
        
        
        
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }
            //my custom code
                $ordered_quantity  = $product  -> getQuantityOrdered();
                $sales_target      = $product  -> getSalesTarget();
                
                $rest_quantity     = $sales_target - $ordered_quantity;
                
                $quote = Mage::getSingleton('checkout/session')->getQuote();
                $pro   = array();
                $add = 0;
                if ( $quote->hasProductId( $product -> getId() )) {
                    
                    foreach( $quote->getAllVisibleItems() as $val ){
                        
                         
                             
                             $added_quantity = $added_quantity + ( $val -> getQty() );
                             $add = 1;
                         
                        
                        //echo $val -> getQty();
                    }
                }
                $params['qty'] = $added_quantity + $params['qty'];
                if( $params['qty'] > $rest_quantity ){
                    $msg = "";
                    if( $add == 1 ){
                        $msg = ", you have already added ".$added_quantity." in your cart.";
                    }
                    if($rest_quantity == 0){
                        $this->_getSession()->addError("No more quantity are available to order");
                    }else{
                        $this->_getSession()->addError("Only ".$rest_quantity." quantity are available to order".$msg );
                    }
                    
                    $this->_redirectReferer();
                     return;
                }
            //code end
            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
    }

    
    
     /**
     * Update customer's shopping cart
     */
   /* protected function _updateShoppingCart()
    {
        
        try {
            $cartData = $this->getRequest()->getParam('cart');
            
            if (is_array($cartData)) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $pro = array();
                foreach ( $cartData as $index => $data ) {
                    if ( isset( $data['qty'] ) ) {
                        
                        //my custom code
                        
                         $cart       = Mage::getModel('sales/quote_item')->load($index);
                         $product_id = $cart -> getProductId();
                         
                         $product    = Mage::getModel('catalog/product')->load( $product_id );
                         $ordered_quantity  = $product  -> getQuantityOrdered();
                         $sales_target      = $product  -> getSalesTarget();
            
                         $rest_quantity     = $sales_target - $ordered_quantity;
                         
                         if( in_array($product_id , $pro) ){
                             $rest_quantity = $rest_quantity + $cartData[$index]['qty'];
                         }else{
                             $pro[] = $product_id;
                         }
                         
                         if( $data['qty'] > $rest_quantity ){

                            $this->_getSession()->addError("Only ".$rest_quantity." quantity are available to order");
                            continue;
                            $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                             
                         }
                         
                        //end
                        
                        
                        
                        
                    }
                }
                //exit;
                $cart = $this->_getCart();
                if (! $cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }
                if( $update == 1 ){
                    $cartData = $cart->suggestItemsQty($cartData);
                    $cart->updateItems($cartData)
                        ->save();
                }
            }
            if( $update == 1 ){
            $this->_getSession()->setCartWasUpdated(true);
            }
        } catch (Mage_Core_Exception $e) {
            //$this->_getSession()->addError(Mage::helper('core')->escapeHtml($e->getMessage()));
        } catch (Exception $e) {
            //$this->_getSession()->addException($e, $this->__('Cannot update shopping cart.'));
            Mage::logException($e);
        }
    }*/
    
    
     
   
}