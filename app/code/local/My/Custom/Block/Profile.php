<?php   
class My_Custom_Block_Profile extends Mage_Core_Block_Template{   

//function to get customer detail
    public function getCustomer(){

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerData = Mage::getModel('customer/customer')->load($customer->getId());
        

        return $customerData;

    }

 //function to get designer gallery
    public function getGallery(){
        
        $customer    = Mage::getSingleton('customer/session')->getCustomer();
        $customer_id = $customer->getId();
        
        $gallery     = Mage::getModel('designergallery/designergallery')->getCollection()->addFieldToFilter('customer_id',$customer_id);
       //echo $gallery->getSelect()-> __toString();
       //echo $gallery->count();
       
        return $gallery;
        
    }


//function to get designer videos
    public function getVideos(){
        $customer    = Mage::getSingleton('customer/session')->getCustomer();
        $customer_id = $customer->getId();
        
        $video       = Mage::getModel('designervideo/designervideo')->getCollection()->addFieldToFilter('customer_id',$customer_id);
        
        return $video;
    }
}

