<?php
class My_Customer_AjaxController extends Mage_Core_Controller_Front_Action
{
     public function getStateAction(){
       $country_code = Mage::app()->getRequest()->getParam('country_code'); 
       
       $output = "";
       $stateCollection =  Mage::getModel('directory/region')->getResourceCollection()->addCountryFilter($country_code)->load();
  
       $output.=  '<select  id="customer_state" class="input-text required-entry validate-select" name="customer_state">';
       $output.=  '<option value="">Select State</option>';
                            
        foreach ($stateCollection as $state) 
        {
                 $sid   = $state->getCode();
                 $sname = $state->getDefaultName();
         
                 $output.=  '<option value="'.$sid.'">'.$sname.'</option>';
                 
        } 
                                    
        $output.= '</select>';

        echo $output;
        exit;
    }
    
    
    // function to add profile picture in designer profile section
    public function uploadProfileAction(){
        
        if($customers = Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customers = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customers->getId();
        }
        //print_r($_FILES);
        //exit;
        
        if (isset($_FILES)){
				
                if ($_FILES['formData']['name']) {

                        if($id){
                                $model = Mage::getModel("customer/customer")->load($id);
                                if($model->getData('customer_profile_image')){
                                    $io = new Varien_Io_File();
                                    $io->rm(Mage::getBaseDir('media').DS.$model->getData('customer_profile_image'));	
                                }
                        }
                        $path = Mage::getBaseDir('media') . DS . 'profile_image' . DS;
                        $uploader = new Varien_File_Uploader('formData');
                        $uploader->setAllowedExtensions(array('jpg','png','jpeg'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $destFile = $path.$_FILES['formData']['name'];
                        $filename = $uploader->getNewFileName($destFile);
                        $result = $uploader->save($path, $filename);

                        $post_data['customer_profile_image'] = 'profile_image/'.$result['file'];
                        
                        $model = Mage::getModel('customer/customer')->load($id)->addData($post_data);
                        $model->setId($id)->save();
                        echo $post_data['customer_profile_image'];
                        exit;
                        
                }
       }
    }
    
    //function add designer gallery for attached images 
    public function addDesignerGalleryAction(){
        
        if($customers = Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customers = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customers->getId();
        }
        //print_r($_FILES);
        //exit;
        
        if (isset($_FILES)){
				
                if ($_FILES['formData']['name']) {

                       /* if($id){
                                $model = Mage::getModel("customer/customer")->load($id);
                                if($model->getData('customer_profile_image')){
                                    $io = new Varien_Io_File();
                                    $io->rm(Mage::getBaseDir('media').DS.$model->getData('customer_profile_image'));	
                                }
                        }*/
                        $path = Mage::getBaseDir('media') . DS . 'designer_gallery_image' . DS;
                        $uploader = new Varien_File_Uploader('formData');
                        $uploader->setAllowedExtensions(array('jpg','png','jpeg'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $destFile = $path.$_FILES['formData']['name'];
                        $filename = $uploader->getNewFileName($destFile);
                        $result = $uploader->save($path, $filename);

                        $post_data['image'] = 'designer_gallery_image/'.$result['file'];
                        $post_data['customer_id'] = $id;
                        $post_data['image_ori_name'] = $_FILES['formData']['name'];
                        $model = Mage::getModel('designergallery/designergallery')->setData($post_data);
                        $insertId = $model->save()->getId();
                        $return['image_name'] = $_FILES['formData']['name'];
                        $return['designer_gallery_id'] = $insertId;
                        $return['image'] = $post_data['image'];
                        
                        echo json_encode($return);
                        exit;
                        
                }
       }
        
    }
    
    
    //function to delete gallery images
    public function deleteGalleryAction(){
        $id = Mage::app()->getRequest()->getParam('gallery_id'); 
          if($id){
            $model = Mage::getModel("designergallery/designergallery")->load($id);
            
            if($model->getData('image')){
                $io = new Varien_Io_File();
                $io->rm(Mage::getBaseDir('media').DS.$model->getData('image'));	
                $model = Mage::getModel('designergallery/designergallery');
                $model->setId($id)->delete();
            }
        }
        
    }
    
    //delete video url from edit profile
    public function deleteVideoAction(){
        
        $id = Mage::app()->getRequest()->getParam('video_id');
        
        if($id){
            
            $model = Mage::getModel('designervideo/designervideo');
            $model->setId($id)->delete();
            
        }
    }
}