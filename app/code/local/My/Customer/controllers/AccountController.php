<?php
/**
 * Customer front controller
 *
 * @category    Customer
 * @package     My Customer
 * @author      Anand Kumar Rai 
 */

require_once Mage::getModuleDir('controllers', 'Mage_Customer') . DS . 'AccountController.php';

class My_Customer_AccountController extends Mage_Customer_AccountController
{
    
    
    
     protected function _getCustomer(){
        
       
        $customer = $this->_getFromRegistry('current_customer');
        
        if (!$customer) {
            $customer = $this->_getModel('customer/customer')->setId(null);
        }
        if ($this->getRequest()->getParam('is_subscribed', false)) {
            $customer->setIsSubscribed(1);
        }
        /**
         * Initialize customer group id
         */
       // $customer->getGroupId();
        
         if($this->getRequest()->getPost('group_id'))
         { 
             $customer->setGroupId($this->getRequest()->getPost('group_id'));
         } else  {
                $customer->getGroupId(); 
         }
         
        return $customer;
    }
    
    /**
     * Customer profile page
     */
    public function profileAction()
    {
       $this->loadLayout();

       //$this->getLayout()->getBlock('customer_profile');
        
       $this->renderLayout();
    }
    
    public function profilePostAction(){
        // $id = $this->getRequest()->getParam('id');
        
        if($customers = Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customers = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customers->getId();
        }
        
        
       $profile_type = $this->getRequest()->getPost('profile_type');
       $data = array();
       if($profile_type == "personal"){
           
            $data = array(
                'customer_title'     => $this->getRequest()->getPost('customer_title'),
                'firstname'          => $this->getRequest()->getPost('firstname'),
                'lastname'           => $this->getRequest()->getPost('lastname'),
                'gender'             => $this->getRequest()->getPost('gender'),
                'customer_address'   => $this->getRequest()->getPost('customer_address'),
                'customer_country'   => $this->getRequest()->getPost('customer_country'),
                'customer_state'     => $this->getRequest()->getPost('customer_state'),
                'customer_city'      => $this->getRequest()->getPost('customer_city'),
                'customer_zipcode'   => $this->getRequest()->getPost('customer_zipcode'),
                'customer_telephone' => $this->getRequest()->getPost('customer_telephone')
                );
            
       }else if($profile_type == "designer"){
           
           $data = array(
                'customer_about_me'    => $this->getRequest()->getPost('customer_about_me'),
                'customer_brand_name'  => $this->getRequest()->getPost('customer_brand_name'),
                'customer_website'     => $this->getRequest()->getPost('customer_website'),
                'customer_facebook'    => $this->getRequest()->getPost('customer_facebook'),
                'customer_twitter'     => $this->getRequest()->getPost('customer_twitter'),
                'customer_linkedin'    => $this->getRequest()->getPost('customer_linkedin'),
                'customer_pinterest'   => $this->getRequest()->getPost('customer_pinterest'),
                'customer_profile_image'   => $this->getRequest()->getPost('customer_profile_image'),
                'account_owner_name'       => $this->getRequest()->getPost('account_owner_name'),
                'bank_name'                => $this->getRequest()->getPost('bank_name'),
                'iban'                     => $this->getRequest()->getPost('iban'),
                'bic'                      => $this->getRequest()->getPost('bic')
                );
           
           
           if(is_array($this->getRequest()->getPost('video_url')) && count($this->getRequest()->getPost('video_url')) > 0){
               
               $video_url = $this->getRequest()->getPost('video_url');
               $video_id  = $this->getRequest()->getPost('video_id');
               //print_r($video_url);exit;
               
               foreach($video_url as $key => $value){
                   
                   if($value != ""){
                        $video_data['url']         =  $value;
                        $video_data['customer_id'] =  $id;
                        if($video_id[$key] == ""){
                            $model = Mage::getModel('designervideo/designervideo')->setData($video_data);
                            $model->save();
                        }else{
                            $model = Mage::getModel('designervideo/designervideo')->load($video_id[$key])->addData($video_data);
                            $model->setId($video_id[$key])->save();
                        }
                        
                   }
               }
               
           }
       }
       
            $model = Mage::getModel('customer/customer')->load($id)->addData($data);
            try {
                $model->setId($id)->save();
                Mage::getSingleton('core/session')->addSuccess('Profile Updated Successfully'); 
                $sucUrl = $this->_getUrl('*/*/profile', array('_secure' => true));
                $this->_redirectError($sucUrl);
            } catch (Exception $e){
                    echo $e->getMessage(); 
            } 
    }
    
    
    /**
     * Create customer account action
     */
    public function createPostAction()
    {
        
        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session->setEscapeMessages(true); // prevent XSS injection in user input
        if (!$this->getRequest()->isPost()) {
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
            return;
        }
        
        $customer = $this->_getCustomer();
        
        try {
            $errors = $this->_getCustomerErrors($customer);
            
            if (empty($errors)) {
                
                $customer->cleanPasswordsValidationData();
                $customer->setData('gender', $this->getRequest()->getPost('gender'));
                $customer->save();
                $this->_dispatchRegisterSuccess($customer);
                Mage::getSingleton('core/session')->setPassword($this->getRequest()->getPost('password'));
                $this->_successProcessRegistration($customer);
                return;
            } else {
                $this->_addSessionError($errors);
            }
        } catch (Mage_Core_Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $url = $this->_getUrl('customer/account/forgotpassword');
                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                $session->setEscapeMessages(false);
            } else {
                $message = $e->getMessage();
            }
            $session->addError($message);
        } catch (Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost())
                ->addException($e, $this->__('Cannot save the customer.'));
        }
        $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
        $this->_redirectError($errUrl);
    }
    
    
    
    
    
     /**
     * Success Registration
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_AccountController
     */
    protected function _successProcessRegistration(Mage_Customer_Model_Customer $customer)
    {
        $session = $this->_getSession();
        if ($customer->isConfirmationRequired()) {
            /** @var $app Mage_Core_Model_App */
            $app = $this->_getApp();
            /** @var $store  Mage_Core_Model_Store*/
            $store = $app->getStore();
            $customer->setPassword(Mage::getSingleton('core/session')->getPassword());
            $customer->sendNewAccountEmail(
                'confirmation',
                $session->getBeforeAuthUrl(),
                $store->getId()
            );
            $customerHelper = $this->_getHelper('customer');
            $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.',
                $customerHelper->getEmailConfirmationUrl($customer->getEmail())));
            $url = $this->_getUrl('*/*/index', array('_secure' => true));
        } else {
            $session->setCustomerAsLoggedIn($customer);
            $url = $this->_welcomeCustomer($customer);
        }
        
        $this->_redirectSuccess($url);
        return $this;
    }
    
    
}