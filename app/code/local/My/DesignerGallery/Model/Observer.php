<?php
class My_DesignerGallery_Model_Observer {
 
    
    //function to delete campaign step session
    public function customer_when_logout(){
        
        Mage::getSingleton('core/session')->unsStepOne();
        Mage::getSingleton('core/session')->unsStepTwo();
    
        $raw_design = Mage::getSingleton('core/session')->getRawDesign();
        
        if(isset($raw_design) && count($raw_design) > 0){
            foreach($raw_design as $key => $value){
             $io = new Varien_Io_File();
             $io->rm(Mage::getBaseDir('media').DS.'product_rawdesign'.DS.$key);
            }
        }
         Mage::getSingleton('core/session')->unsRawDesign();
         
        $product_image = Mage::getSingleton('core/session')->getProductImage();
        
        if(isset($product_image) && count($product_image) > 0){
            foreach($product_image as $key => $value){
             $io = new Varien_Io_File();
             $io->rm(Mage::getBaseDir('media').DS.'import'.DS.$key);
            }
        }
        
         Mage::getSingleton('core/session')->unsProductImage();
    }
}