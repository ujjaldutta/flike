<?php
class My_DesignerGallery_Model_Mysql4_Designergallery extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("designergallery/designergallery", "designer_gallery_id");
    }
}