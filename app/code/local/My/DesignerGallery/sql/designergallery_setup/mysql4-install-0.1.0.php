<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table designer_gallery(designer_gallery_id int not null auto_increment, 
customer_id int  NOT NULL,
image varchar(255) NOT NULL,
image_ori_name varchar(255) NOT NULL,
 primary key(designer_gallery_id));
		
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 