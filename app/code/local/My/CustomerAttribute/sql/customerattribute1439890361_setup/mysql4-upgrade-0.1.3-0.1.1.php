<?php
$installer = $this;
$installer->startSetup();

$installer->removeAttribute('customer', 'customer_city');

$installer->addAttribute("customer", "customer_city",  array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "City",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => true,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => ""

	));

$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "customer_city");
$attribute->save();     
	
$installer->endSetup();
	 