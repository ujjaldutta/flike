<?php
class My_SpecialFeature_Model_Observer {
    
    
    public function overridefield($observer) {
        $form = $observer->getEvent()->getForm();
        
	
	
	$packagetype = $form->getElement('product_features');
	if($packagetype){
	    
	$packagetype->setRenderer(
                Mage::app()->getLayout()->createBlock('specialfeature/adminhtml_product_features')
            );
	}
        
        $packagetype = $form->getElement('user_id');
	if($packagetype){
	    
	$packagetype->setRenderer(
                Mage::app()->getLayout()->createBlock('specialfeature/adminhtml_user_detail')
            );
	}
	
        $packagetype = $form->getElement('profit_price');
        if( $packagetype ){
            $profit_price = $form->getElement('profit_price')->getValue();
            Mage::getSingleton('core/session')->setProfitPrice( $profit_price );
        }
	
    }
    
   
    public function saveFeatures($observer) {
	$data=Mage::app()->getRequest()->getParams();
	$product = $observer->getEvent()->getProduct();
	echo "saved;";
    }
    
   
}