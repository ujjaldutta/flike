<?php
class My_SpecialFeature_Block_Adminhtml_User_Detail
    extends Mage_Adminhtml_Block_Widget
    implements Varien_Data_Form_Element_Renderer_Interface {
    public function __construct()
    {
        $this->setTemplate('user_detail/index.phtml'); //set a template
    }
    public function render(Varien_Data_Form_Element_Abstract $element) {
        $this->setElement($element);
        return $this->toHtml();
    }
    
    public function getUserDetail($product_id){
        
       $usr_product =  Mage::getModel('userproduct/userproduct')->load($product_id, 'product_id');
       $customerData = Mage::getModel('customer/customer')->load($usr_product->getCustomerId())->getData();
       
       return $customerData;
    }
    
}