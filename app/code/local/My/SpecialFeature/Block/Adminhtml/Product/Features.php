<?php
class My_SpecialFeature_Block_Adminhtml_Product_Features
    extends Mage_Adminhtml_Block_Widget
    implements Varien_Data_Form_Element_Renderer_Interface {
    public function __construct()
    {
        $this->setTemplate('specialfeatures/index.phtml'); //set a template
    }
    public function render(Varien_Data_Form_Element_Abstract $element) {
        $this->setElement($element);
        return $this->toHtml();
    }
    
    
    public function getSpecialFeatures($product_id){
        
        $special_feature = Mage::getModel('specialfeature/specialfeature')->getCollection()->addFieldToFilter('product_id',$product_id);
        return $special_feature;
    }
    
    public function getRawDesign($product_id){
        
        $raw_design = Mage::getModel('rawdesign/rawdesign')->getCollection()->addFieldToFilter('product_id',$product_id);
        return $raw_design;
        
    }
    
    public function getProductFeatures($product_id){
        
       $product_feature = Mage::getModel('featuredescription/featuredescription')->getCollection()->addFieldToFilter('product_id',$product_id);
       return $product_feature;
        
    }
    
    public function getColorDescription($product_id){
        
        $color_description = Mage::getModel('colordescription/colordescription')->getCollection()->addFieldToFilter('product_id',$product_id);
       return $color_description;
       
    }
    
  
}