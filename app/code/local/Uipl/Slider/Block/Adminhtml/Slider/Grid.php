<?php
/**
 * Uipl_Slider extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Uipl
 * @package        Uipl_Slider
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slider admin grid block
 *
 * @category    Uipl
 * @package     Uipl_Slider
 * @author      Ultimate Module Creator
 */
class Uipl_Slider_Block_Adminhtml_Slider_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('sliderGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Uipl_Slider_Block_Adminhtml_Slider_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('uipl_slider/slider')
            ->getCollection()
            ->addAttributeToSelect('slider_category')
            ->addAttributeToSelect('status');
        
        $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
        $store = $this->_getStore();
        $collection->joinAttribute(
            'image_name', 
            'uipl_slider_slider/image_name', 
            'entity_id', 
            null, 
            'inner', 
            $adminStore
        );
        if ($store->getId()) {
            $collection->joinAttribute('
                uipl_slider_slider_image_name', 
                'uipl_slider_slider/image_name', 
                'entity_id', 
                null, 
                'inner', 
                $store->getId()
            );
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Uipl_Slider_Block_Adminhtml_Slider_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('uipl_slider')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'image_name',
            array(
                'header'    => Mage::helper('uipl_slider')->__('Name'),
                'align'     => 'left',
                'index'     => 'image_name',
            )
        );
        
        if ($this->_getStore()->getId()) {
            $this->addColumn(
                'uipl_slider_slider_image_name', 
                array(
                    'header'    => Mage::helper('uipl_slider')->__('Name in %s', $this->_getStore()->getName()),
                    'align'     => 'left',
                    'index'     => 'uipl_slider_slider_image_name',
                )
            );
        }

        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('uipl_slider')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('uipl_slider')->__('Enabled'),
                    '0' => Mage::helper('uipl_slider')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'slider_category',
            array(
                'header' => Mage::helper('uipl_slider')->__('Select Category'),
                'index'  => 'slider_category',
                'type'  => 'options',
                'options' => Mage::helper('uipl_slider')->convertOptions(
                    Mage::getModel('eav/config')->getAttribute('uipl_slider_slider', 'slider_category')->getSource()->getAllOptions(false)
                )

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('uipl_slider')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('uipl_slider')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('uipl_slider')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('uipl_slider')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('uipl_slider')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('uipl_slider')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('uipl_slider')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * get the selected store
     *
     * @access protected
     * @return Mage_Core_Model_Store
     * @author Ultimate Module Creator
     */
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Uipl_Slider_Block_Adminhtml_Slider_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('slider');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('uipl_slider')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('uipl_slider')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('uipl_slider')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('uipl_slider')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('uipl_slider')->__('Enabled'),
                            '0' => Mage::helper('uipl_slider')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $this->getMassactionBlock()->addItem(
            'slider_category',
            array(
                'label'      => Mage::helper('uipl_slider')->__('Change Select Category'),
                'url'        => $this->getUrl('*/*/massSliderCategory', array('_current'=>true)),
                'additional' => array(
                    'flag_slider_category' => array(
                        'name'   => 'flag_slider_category',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('uipl_slider')->__('Select Category'),
                        'values' => Mage::getModel('eav/config')->getAttribute('uipl_slider_slider', 'slider_category')
                            ->getSource()->getAllOptions(true),

                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Uipl_Slider_Model_Slider
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}
