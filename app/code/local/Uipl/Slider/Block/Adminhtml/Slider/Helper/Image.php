<?php
/**
 * Uipl_Slider extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Uipl
 * @package        Uipl_Slider
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slider image field renderer helper
 *
 * @category    Uipl
 * @package     Uipl_Slider
 * @author      Ultimate Module Creator
 */
class Uipl_Slider_Block_Adminhtml_Slider_Helper_Image extends Varien_Data_Form_Element_Image
{
    /**
     * get the url of the image
     *
     * @access protected
     * @return string
     * @author Ultimate Module Creator
     */
    protected function _getUrl()
    {
        $url = false;
        if ($this->getValue()) {
            $url = Mage::helper('uipl_slider/slider_image')->getImageBaseUrl().
                $this->getValue();
        }
        return $url;
    }
}
