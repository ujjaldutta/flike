<?php
/**
 * Uipl_Slider extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Uipl
 * @package        Uipl_Slider
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slider admin edit tabs
 *
 * @category    Uipl
 * @package     Uipl_Slider
 * @author      Ultimate Module Creator
 */
class Uipl_Slider_Block_Adminhtml_Slider_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('slider_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('uipl_slider')->__('Slider Information'));
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return Uipl_Slider_Block_Adminhtml_Slider_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _prepareLayout()
    {
        $slider = $this->getSlider();
        $entity = Mage::getModel('eav/entity_type')
            ->load('uipl_slider_slider', 'entity_type_code');
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($entity->getEntityTypeId());
        $attributes->getSelect()->order('additional_table.position', 'ASC');

        $this->addTab(
            'info',
            array(
                'label'   => Mage::helper('uipl_slider')->__('Slider Information'),
                'content' => $this->getLayout()->createBlock(
                    'uipl_slider/adminhtml_slider_edit_tab_attributes'
                )
                ->setAttributes($attributes)
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve slider entity
     *
     * @access public
     * @return Uipl_Slider_Model_Slider
     * @author Ultimate Module Creator
     */
    public function getSlider()
    {
        return Mage::registry('current_slider');
    }
}
