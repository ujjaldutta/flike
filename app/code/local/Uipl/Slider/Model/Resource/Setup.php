<?php
/**
 * Uipl_Slider extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Uipl
 * @package        Uipl_Slider
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slider setup
 *
 * @category    Uipl
 * @package     Uipl_Slider
 * @author      Ultimate Module Creator
 */
class Uipl_Slider_Model_Resource_Setup extends Mage_Catalog_Model_Resource_Setup
{

    /**
     * get the default entities for slider module - used at installation
     *
     * @access public
     * @return array()
     * @author Ultimate Module Creator
     */
    public function getDefaultEntities()
    {
        $entities = array();
        $entities['uipl_slider_slider'] = array(
            'entity_model'                  => 'uipl_slider/slider',
            'attribute_model'               => 'uipl_slider/resource_eav_attribute',
            'table'                         => 'uipl_slider/slider',
            'additional_attribute_table'    => 'uipl_slider/eav_attribute',
            'entity_attribute_collection'   => 'uipl_slider/slider_attribute_collection',
            'attributes'                    => array(
                    'slider_category' => array(
                        'group'          => 'General',
                        'type'           => 'int',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Select Category',
                        'input'          => 'select',
                        'source'         => 'eav/entity_attribute_source_table',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
                        'required'       => '1',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '10',
                        'note'           => 'This is the category for the slider',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                        'option' =>
                            array (
                                'values' =>
                                    array (
                                        '|Publish|Shop|About',
                                    ),
                                ),
                    ),
                    'slider_image' => array(
                        'group'          => 'General',
                        'type'           => 'varchar',
                        'backend'        => 'uipl_slider/slider_attribute_backend_image',
                        'frontend'       => '',
                        'label'          => 'Upload Image',
                        'input'          => 'image',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
                        'required'       => '',
                        'user_defined'   => true,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '20',
                        'note'           => 'Upload image for the slider',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),
                    'image_name' => array(
                        'group'          => 'General',
                        'type'           => 'varchar',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Name',
                        'input'          => 'text',
                        'source'         => '',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
                        'required'       => '1',
                        'user_defined'   => false,
                        'default'        => '',
                        'unique'         => false,
                        'position'       => '30',
                        'note'           => 'Name for the Image',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),
                    'status' => array(
                        'group'          => 'General',
                        'type'           => 'int',
                        'backend'        => '',
                        'frontend'       => '',
                        'label'          => 'Enabled',
                        'input'          => 'select',
                        'source'         => 'eav/entity_attribute_source_boolean',
                        'global'         => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'required'       => '',
                        'user_defined'   => false,
                        'default'        => '1',
                        'unique'         => false,
                        'position'       => '40',
                        'note'           => '',
                        'visible'        => '1',
                        'wysiwyg_enabled'=> '0',
                    ),

                )
         );
        return $entities;
    }
}
