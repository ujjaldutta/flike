<?php
/**
 * Uipl_Slider extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Uipl
 * @package        Uipl_Slider
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    Uipl
 * @package     Uipl_Slider
 * @author      Ultimate Module Creator
 */
class Uipl_Slider_Model_Adminhtml_Search_Slider extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Uipl_Slider_Model_Adminhtml_Search_Slider
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('uipl_slider/slider_collection')
            ->addAttributeToFilter('image_name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $slider) {
            $arr[] = array(
                'id'          => 'slider/1/'.$slider->getId(),
                'type'        => Mage::helper('uipl_slider')->__('Slider'),
                'name'        => $slider->getImageName(),
                'description' => $slider->getImageName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/slider_slider/edit',
                    array('id'=>$slider->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
