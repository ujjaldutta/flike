<?php
/**
 * Uipl_Slider extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Uipl
 * @package        Uipl_Slider
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Slider admin controller
 *
 * @category    Uipl
 * @package     Uipl_Slider
 * @author      Ultimate Module Creator
 */
class Uipl_Slider_Adminhtml_Slider_SliderController extends Mage_Adminhtml_Controller_Action
{
    /**
     * constructor - set the used module name
     *
     * @access protected
     * @return void
     * @see Mage_Core_Controller_Varien_Action::_construct()
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->setUsedModuleName('Uipl_Slider');
    }

    /**
     * init the slider
     *
     * @access protected 
     * @return Uipl_Slider_Model_Slider
     * @author Ultimate Module Creator
     */
    protected function _initSlider()
    {
        $this->_title($this->__('Slider'))
             ->_title($this->__('Manage Sliders'));

        $sliderId  = (int) $this->getRequest()->getParam('id');
        $slider    = Mage::getModel('uipl_slider/slider')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if ($sliderId) {
            $slider->load($sliderId);
        }
        Mage::register('current_slider', $slider);
        return $slider;
    }

    /**
     * default action for slider controller
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->_title($this->__('Slider'))
             ->_title($this->__('Manage Sliders'));
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * new slider action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * edit slider action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $sliderId  = (int) $this->getRequest()->getParam('id');
        $slider    = $this->_initSlider();
        if ($sliderId && !$slider->getId()) {
            $this->_getSession()->addError(
                Mage::helper('uipl_slider')->__('This slider no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        if ($data = Mage::getSingleton('adminhtml/session')->getSliderData(true)) {
            $slider->setData($data);
        }
        $this->_title($slider->getImageName());
        Mage::dispatchEvent(
            'uipl_slider_slider_edit_action',
            array('slider' => $slider)
        );
        $this->loadLayout();
        if ($slider->getId()) {
            if (!Mage::app()->isSingleStoreMode() && ($switchBlock = $this->getLayout()->getBlock('store_switcher'))) {
                $switchBlock->setDefaultStoreName(Mage::helper('uipl_slider')->__('Default Values'))
                    ->setWebsiteIds($slider->getWebsiteIds())
                    ->setSwitchUrl(
                        $this->getUrl(
                            '*/*/*',
                            array(
                                '_current'=>true,
                                'active_tab'=>null,
                                'tab' => null,
                                'store'=>null
                            )
                        )
                    );
            }
        } else {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->renderLayout();
    }

    /**
     * save slider action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        $storeId        = $this->getRequest()->getParam('store');
        $redirectBack   = $this->getRequest()->getParam('back', false);
        $sliderId   = $this->getRequest()->getParam('id');
        $isEdit         = (int)($this->getRequest()->getParam('id') != null);
        $data = $this->getRequest()->getPost();
        if ($data) {
            $slider     = $this->_initSlider();
            $sliderData = $this->getRequest()->getPost('slider', array());
            $slider->addData($sliderData);
            $slider->setAttributeSetId($slider->getDefaultAttributeSetId());
            if ($useDefaults = $this->getRequest()->getPost('use_default')) {
                foreach ($useDefaults as $attributeCode) {
                    $slider->setData($attributeCode, false);
                }
            }
            try {
                $slider->save();
                $sliderId = $slider->getId();
                $this->_getSession()->addSuccess(
                    Mage::helper('uipl_slider')->__('Slider was saved')
                );
            } catch (Mage_Core_Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage())
                    ->setSliderData($sliderData);
                $redirectBack = true;
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError(
                    Mage::helper('uipl_slider')->__('Error saving slider')
                )
                ->setSliderData($sliderData);
                $redirectBack = true;
            }
        }
        if ($redirectBack) {
            $this->_redirect(
                '*/*/edit',
                array(
                    'id'    => $sliderId,
                    '_current'=>true
                )
            );
        } else {
            $this->_redirect('*/*/', array('store'=>$storeId));
        }
    }

    /**
     * delete slider
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $slider = Mage::getModel('uipl_slider/slider')->load($id);
            try {
                $slider->delete();
                $this->_getSession()->addSuccess(
                    Mage::helper('uipl_slider')->__('The sliders has been deleted.')
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->getResponse()->setRedirect(
            $this->getUrl('*/*/', array('store'=>$this->getRequest()->getParam('store')))
        );
    }

    /**
     * mass delete sliders
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $sliderIds = $this->getRequest()->getParam('slider');
        if (!is_array($sliderIds)) {
            $this->_getSession()->addError($this->__('Please select sliders.'));
        } else {
            try {
                foreach ($sliderIds as $sliderId) {
                    $slider = Mage::getSingleton('uipl_slider/slider')->load($sliderId);
                    Mage::dispatchEvent(
                        'uipl_slider_controller_slider_delete',
                        array('slider' => $slider)
                    );
                    $slider->delete();
                }
                $this->_getSession()->addSuccess(
                    Mage::helper('uipl_slider')->__('Total of %d record(s) have been deleted.', count($sliderIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $sliderIds = $this->getRequest()->getParam('slider');
        if (!is_array($sliderIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('uipl_slider')->__('Please select sliders.')
            );
        } else {
            try {
                foreach ($sliderIds as $sliderId) {
                $slider = Mage::getSingleton('uipl_slider/slider')->load($sliderId)
                    ->setStatus($this->getRequest()->getParam('status'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d sliders were successfully updated.', count($sliderIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('uipl_slider')->__('There was an error updating sliders.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * restrict access
     *
     * @access protected
     * @return bool
     * @see Mage_Adminhtml_Controller_Action::_isAllowed()
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/uipl_slider/slider');
    }

    /**
     * Export sliders in CSV format
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'sliders.csv';
        $content    = $this->getLayout()->createBlock('uipl_slider/adminhtml_slider_grid')
            ->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export sliders in Excel format
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'slider.xls';
        $content    = $this->getLayout()->createBlock('uipl_slider/adminhtml_slider_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export sliders in XML format
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'slider.xml';
        $content    = $this->getLayout()->createBlock('uipl_slider/adminhtml_slider_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * wysiwyg editor action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function wysiwygAction()
    {
        $elementId     = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId       = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock(
            'uipl_slider/adminhtml_slider_helper_form_wysiwyg_content',
            '',
            array(
                'editor_element_id' => $elementId,
                'store_id'          => $storeId,
                'store_media_url'   => $storeMediaUrl,
            )
        );
        $this->getResponse()->setBody($content->toHtml());
    }

    /**
     * mass Select Category change
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massSliderCategoryAction()
    {
        $sliderIds = (array)$this->getRequest()->getParam('slider');
        $storeId       = (int)$this->getRequest()->getParam('store', 0);
        $flag          = (int)$this->getRequest()->getParam('flag_slider_category');
        if ($flag == 2) {
            $flag = 0;
        }
        try {
            foreach ($sliderIds as $sliderId) {
                $slider = Mage::getSingleton('uipl_slider/slider')
                    ->setStoreId($storeId)
                    ->load($sliderId);
                $slider->setSliderCategory($flag)->save();
            }
            $this->_getSession()->addSuccess(
                Mage::helper('uipl_slider')->__('Total of %d record(s) have been updated.', count($sliderIds))
            );
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException(
                $e,
                Mage::helper('uipl_slider')->__('An error occurred while updating the sliders.')
            );
        }
        $this->_redirect('*/*/', array('store'=> $storeId));
    }
}
