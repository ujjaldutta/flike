<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table faq_category(id int not null auto_increment, cat_name varchar(100),status int(11) COMMENT '0=disable,1=enable.', post_date datetime, primary key(id));


    insert into faq_category values(1,'Cat1','1',"2014-12-31 10:00:00");
    insert into faq_category values(2,'Cat2','1',"2014-12-31 11:00:00");
    insert into faq_category values(3,'Cat3','1',"2014-12-31 11:30:00");
    
    
 
create table faq_master(id int not null auto_increment, cat_id int(11),question text,answer text, status int(11) COMMENT '0=disable,1=enable.', post_date datetime, primary key(id));


    insert into faq_master values(1,1,'How can we modify search','We can customize it by backend',1,"2014-12-31 10:00:00");
    insert into faq_master values(2,1,'How can we modify Item','We can customize it by backend',1,"2014-12-31 11:00:00");
    insert into faq_master values(3,2,'How can we modify Total Price','We can customize it by backend',1,"2014-12-31 11:30:00");
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 