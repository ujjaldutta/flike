<?php

class Uipl_Faq_Block_Index extends Mage_Core_Block_Template {

    public function showFaq() {


        $category = Mage::getModel("faq/faqcategory")->getCollection()->addFieldToFilter('status', 1);

        $i = 0;
        foreach ($category as $obj) {
            //print_r('$obj')  ;
            $data_array1[$i] = $obj->getData();
            $data_array1[$i]['cat_name'] = $obj->getData('cat_name');
            $i++;
        }
        return $data_array1;

       
    }

    public function showQues($id) {
        $faq = Mage::getModel("faq/faq")->getCollection()->addFilter('cat_id', $id);
        $i = 0;
        foreach ($faq as $obj) {

            $data_array1[$i] = $obj->getData();
            $data_array1[$i]['cat_dtls'] = $obj->getData();
            $i++;
            
        }

        return $data_array1;

    }
public function showAns($id) {
         $faq = Mage::getModel("faq/faq")->getCollection()->addFilter('id', $id);
        
        return $faq->getData();

    }
}
