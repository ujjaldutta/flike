<?php

class Uipl_Faq_Block_Adminhtml_Faq_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("faqGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
                    echo 'hiiiii';
				$collection = Mage::getModel("faq/faq")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("faq")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
						$this->addColumn('cat_id', array(
						'header' => Mage::helper('faq')->__('Category'),
						'index' => 'cat_id',
						'type' => 'options',
						'options'=>Uipl_Faq_Block_Adminhtml_Faq_Grid::getOptionArray3(),				
						));
						
						$this->addColumn("question", array(
						"header" => Mage::helper("faq")->__("Question"),
						"index" => "question",
						));
						
						$this->addColumn("answer", array(
						"header" => Mage::helper("faq")->__("Answer"),
						"index" => "answer",
						));
						
						$this->addColumn('status', array(
						'header' => Mage::helper('faq')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Uipl_Faq_Block_Adminhtml_Faq_Grid::getOptionArray6(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_faq', array(
					 'label'=> Mage::helper('faq')->__('Remove Faq'),
					 'url'  => $this->getUrl('*/adminhtml_faq/massRemove'),
					 'confirm' => Mage::helper('faq')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray3()
		{
				
			$category = Mage::getModel("faq/faqcategory")->getCollection();
			$i=0;
			foreach($category as $obj){
				$data_array[$obj->getData('id')]=$obj->getData('cat_name');
			$i++;    
		       }  
			//$all_ids = $category->getCollection()->getAllIds();
			//print_r($all_ids);	
				
		//	$read = Mage::getSingleton('core/resource')->getConnection('core_read');
		//
		//		$readresult = $read->query("SELECT question FROM faq_master");
		//		
		//		while ($row = $readresult->fetch() ) {
		//		    $Ids[] = $row['question'];
		//		}	
		//		print_r($Ids);
		//		
		//        $data_array=array(); 
		//	$data_array[0]='0';
		//	$data_array[1]='1';
		        return($data_array);
		}
		static public function getValueArray3()
		{
		$data_array=array();
			    foreach(Uipl_Faq_Block_Adminhtml_Faq_Grid::getOptionArray3() as $k=>$v){
				$data_array[]=array('value'=>$k,'label'=>$v);		
			    }
		return($data_array);

		}
		
		static public function getOptionArray6()
		{
		$data_array=array(); 
			$data_array[0]='0';
			$data_array[1]='1';
		return($data_array);
		}
		static public function getValueArray6()
		{
		$data_array=array();
			foreach(Uipl_Faq_Block_Adminhtml_Faq_Grid::getOptionArray6() as $k=>$v){
				$data_array[]=array('value'=>$k,'label'=>$v);		
			}
		return($data_array);

		}
		

}
