<?php

class Uipl_Faq_IndexController extends Mage_Core_Controller_Front_Action {

    public function IndexAction() {

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("FAQ"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("faq", array(
            "label" => $this->__("FAQ"),
            "title" => $this->__("FAQ")
        ));

        $this->renderLayout();
    }

    public function DetailsAction() {


        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("FAQ"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("faq", array(
            "label" => $this->__("FAQ"),
            "title" => $this->__("FAQ")
        ));

        $this->renderLayout();
    }

    public function SearchAction() {

        $data = $this->getRequest()->getParam('term');
        $faqs = Mage::getModel("faq/faq")->getCollection()->addFieldToFilter(array('question'), array(
                    array('like' => '%' . $data . '%'), //spaces on each side
                ))->setOrder('cat_id', 'DESC');

        //echo $faqs->getSelect()->__toString();
        //echo $faqs->count();
        //exit();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $categoryarray = array();
        $query = 'SELECT * FROM faq_category where status=1';
        $catres1 = $readConnection->fetchAll($query);

        foreach ($catres1 as $catres1) {
            $categoryarray[] = $catres1['cat_name'];
        }
        $q = array();
       
        foreach ($faqs as $faq) {
            $categoryId = $faq->cat_id;
            $query = 'SELECT * FROM faq_category where id=' . $categoryId;
            $catres = $readConnection->fetchRow($query);
            
            $url = Mage::getUrl('faqs/index/details',array("id" => $faq->id)) ;
            $q[$faq->cat_id][] = array("url" => $url, "question" => $faq->question, "cat_name" => $catres['cat_name']);
        }
       
        $data1 = array();
        foreach ($q as $key => $data) {

            $i = 1;
            foreach ($data as $eachData) {

                $html = '';
                if ($i == 1) {
                    //echo 'ok';
                    $html = '<div class="cathead">' . $eachData['cat_name'] . '</div>';
                }

                $html .='<div class="ques1">' . $eachData['question'] . '</div>';
                $data1[] = array("value" => $eachData['question'], "label" => $html, "url" => $eachData['url']);
                //print_r($data);
                // echo json_encode($data1);
                // exit;   
                $i++;
            }
        }

        echo json_encode($data1);
        exit;
    }

}
