<?php
/*
* Controller class has to be inherited from Mage_Core_Controller_action
*/
class MyCustomModule_HelloWorld_IndexController extends Mage_Core_Controller_Front_Action
{

    
    
    
/*
* this method privides default action.
*/
public function indexAction()
{
/*
* Initialization of Mage_Core_Model_Layout model
*/
$this->loadLayout();

/*
* Building page according to layout confuration
*/
$this->renderLayout();
}


/*
* this method privides default action.
*/
public function addAction()
{
$this->loadLayout();

        $this->getLayout()->getBlock('addform');
        //$this->_getSession()->unsForgottenEmail();

       // $this->_initLayoutMessages('helloworld/session');
        $this->renderLayout();
}

public function addPostAction(){
    
     $first_name =  $this->getRequest()->getPost('first_name');
     $last_name  =  $this->getRequest()->getPost('last_name');
     $email      =  $this->getRequest()->getPost('email');
    
    $data = array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email);
    $model = Mage::getModel('mycustommodule_helloworld/helloworld');
    
    $model->setData('first_name', $first_name);
    $model->setData('last_name', $last_name);
    $model->setData('email', $email);
    $model -> save();
    $this->_redirect('helloworld/index/add');
    exit;
    }


}