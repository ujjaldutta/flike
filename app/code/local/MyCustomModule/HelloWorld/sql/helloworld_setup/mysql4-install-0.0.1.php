<?php
$installer = $this;
$installer->startSetup();
$installer->run("-- DROP TABLE IF EXISTS {$this->getTable('helloworld')};
CREATE TABLE {$this->getTable('helloworld')} (
      `first_name` varchar(255) NOT NULL default '',
      `last_name` varchar(255) NOT NULL default '',
      `email` varchar(255) NOT NULL default ''
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
 $installer->endSetup();