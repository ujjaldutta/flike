<?php 
class MyCustomModule_HelloWorld_Model_Resource_HelloWorld_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract{
    
    protected function _construct(){
        $this -> _init("helloworld/helloworld");
    }
}
