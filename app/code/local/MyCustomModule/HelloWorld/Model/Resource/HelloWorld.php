<?php
class MyCustomModule_HelloWorld_Model_Resource_HelloWorld extends Mage_Core_Model_Resource_Db_Abstract{
    
    protected function _construct(){
        //$this -> _init("mycustommodule_helloworld/helloworld", "helloworld_id");
         $resource = Mage::getSingleton('core/resource');
        $this->setType('mycustommodule_helloworld/helloworld')
            ->setConnection(
                $resource->getConnection('helloworld_read'),
                $resource->getConnection('helloworld_write')
            );
    }
}