<?php

class My_Campaign_CreateController extends Mage_Core_Controller_Front_Action {

    public function IndexAction() {

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("titlename", array(
            "label" => $this->__("Titlename"),
            "title" => $this->__("Titlename")
        ));

        $this->renderLayout();
    }

    public function SteponeAction() {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('');
        }

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Step One"));

        //if the form is submitted
        //end
        $this->renderLayout();
    }

    public function SteponePostAction() {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('');
        }

        $campaign_name = $this->getRequest()->getPost('campaign_name');
        $category_id = $this->getRequest()->getPost('category_id');
        $subcategory_id = $this->getRequest()->getPost('subcategory_id');
        $product_type = $this->getRequest()->getPost('product_type');

        $arr['campaign_name'] = $campaign_name;
        $arr['category_id'] = $category_id;
        $arr['subcategory_id'] = $subcategory_id;
        $arr['product_type'] = $product_type;



        Mage::getSingleton('core/session')->setStepOne($arr);
        Mage::getSingleton('core/session')->setProductType($arr['product_type']);
        $this->_redirect('campaign/create/steptwo');
    }

    public function SteptwoAction() {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('');
        }

        $step1 = Mage::getSingleton('core/session')->getStepOne();

        if (empty($step1)) {
            $this->_redirect('campaign/create/stepone');
        }

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Step Two"));
        $this->renderLayout();
    }

    public function SteptwoPostAction() {

        //  $valid = Mage::getSingleton('core/session')->getStepOne();

        /* if(!is_array($valid) || count($valid) == 0){
          $this->_redirect('campaign/create/stepone');
          } */
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('');
        }


        $sales_target = $this->getRequest()->getPost('sales_target');
        $campaign_duration = $this->getRequest()->getPost('campaign_duration');
        $color_value = $this->getRequest()->getPost('color_value');
        $color_description = $this->getRequest()->getPost('color_description');
        $product_features = $this->getRequest()->getPost('product_features');
        $product_description = $this->getRequest()->getPost('product_description');
        $special_features = $this->getRequest()->getPost('special_features');
        $latest_base_price = $this->getRequest()->getPost('latest_base_price');
        $latest_profit_price = $this->getRequest()->getPost('latest_profit_price');
        $color_type = $this->getRequest()->getPost('color_type');

        $arr['sales_target'] = $sales_target;
        $arr['campaign_duration'] = $campaign_duration;
        $arr['color_value'] = $color_value;
        $arr['color_description'] = $color_description;
        $arr['product_features'] = $product_features;
        $arr['product_description'] = $product_description;
        $arr['special_features'] = $special_features;
        $arr['latest_base_price'] = $latest_base_price;
        $arr['latest_profit_price'] = $latest_profit_price;
        $arr['color_type'] = $color_type;

        Mage::getSingleton('core/session')->setStepTwo($arr);
        $this->_redirect('campaign/create/stepthree');
    }

    public function StepthreeAction() {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('');
        }

        $step2 = Mage::getSingleton('core/session')->getStepTwo();

        if (empty($step2)) {
            $this->_redirect('campaign/create/steptwo');
        }
        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Step Three"));
        $this->renderLayout();
    }

    public function PreviewAction() {

         if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('');
        }



        $product_description = $this->getRequest()->getPost('product_description');
        $campaign_url = $this->getRequest()->getPost('campaign_url');



        $arr['product_description'] = $product_description;
        $arr['campaign_url'] = $campaign_url;


        Mage::getSingleton('core/session')->setStepThree($arr);
        

        $this->loadLayout();
       
        

        $this->renderLayout();
    }

    public function PreviewPostAction() {

        //  $valid = Mage::getSingleton('core/session')->getStepOne();

        /* if(!is_array($valid) || count($valid) == 0){
          $this->_redirect('campaign/create/stepone');
          } */
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('');
        }



        $product_description = $this->getRequest()->getPost('product_description');
        $campaign_url = $this->getRequest()->getPost('campaign_url');



        $arr['product_description'] = $product_description;
        $arr['campaign_url'] = $campaign_url;


        Mage::getSingleton('core/session')->setStepThree($arr);
        //$this->_redirect('campaign/create/stepthree');
    }

    public function StepthreePostAction() {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('');
        }

        $product_description = $this->getRequest()->getPost('product_description');
        $campaign_url = $this->getRequest()->getPost('campaign_url');

        $step1 = Mage::getSingleton('core/session')->getStepOne();
        $step2 = Mage::getSingleton('core/session')->getStepTwo();
        $step3 =  Mage::getSingleton('core/session')->getStepThree();
        $product_description = $step3['product_description'];
        //print '<pre>';
        //print_r($step2);
        //exit;
        $raw_design = Mage::getSingleton('core/session')->getRawDesign();

        $product_image = Mage::getSingleton('core/session')->getProductImage();
        // print '<pre>';
        // print_r($step1);
        // print_r($step2);
        //  print_r($raw_design);
        // print_r($product_image);
        // exit;
        //code to get the size option value

        $attribute_code = "size";
        $attribute_details = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $attribute_code);
        $size_attr_id = $attribute_details->getAttributeId();
        $optionsd = $attribute_details->getSource()->getAllOptions(false);
        foreach ($optionsd as $optiond) {
            $size[$optiond["value"]] = $optiond["label"];
        }

        //end
        // print '<pre>';
        // print_r($size);
        //exit;
        
        //option id of attribute code campaign status for value Running.
            $helper            = Mage::helper('campaign');
            $running_option_id = $helper -> getOptionId('campaign_status','Running');
        //end
        
        
        $simplepro = array();
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerData = Mage::getModel('customer/customer')->load($customer->getId())->getData();

        $category = array($step1['category_id'], $step1['subcategory_id'], $step1['product_type']);
        $selcolor = $step2['color_value'];
        // print_r($selcolor);exit;
        // code to get color attribute id
        $arg_attribute = 'color';




        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);




        $attr_id = $attr->getAttributeId();

        $option['attribute_id'] = $attr_id;
        $arg_value = array();
        if(is_array($selcolor) && count($selcolor)>0){
            foreach ($selcolor as $color) {
                if ($color != ''){
                $option['value'][$color][] = $color;
                }
            }
        }

//checking required for existing color
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);


        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $_product = Mage::getModel('catalog/product');
        $attr = $_product->getResource()->getAttribute($arg_attribute);


if(is_array($selcolor) && count($selcolor)>0){
        foreach ($selcolor as $selcolor) {
            if($selcolor != ''){

            if ($attr->usesSource()) {

                $color_id = $attr->getSource()->getOptionId($selcolor);



                foreach ($size as $size_id => $size_val) {
                    $sku = strtolower($customerData['firstname']) . "_" . uniqid();
                    $simpleProduct = Mage::getModel('catalog/product');
                    try {
                        $simpleProduct
                                //    ->setStoreId(1) //you can set data in store scope
                                ->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
                                ->setAttributeSetId(4) //ID of a attribute set named 'default'
                                ->setTypeId('simple') //product type
                                ->setCreatedAt(strtotime('now')) //product creation time
                                //    ->setUpdatedAt(strtotime('now')) //product update time
                                ->setSku($sku) //SKU
                                ->setName($step1['campaign_name']) //product name
                                ->setWeight(0)
                                ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                                ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE) //catalog and search visibility
                                //->setManufacturer(28) //manufacturer id
                                ->setColor($color_id)
                                ->setSize($size_id)
                                // ->setNewsFromDate('06/26/2015') //product set as new from
                                // ->setNewsToDate('06/30/2015') //product set as new to
                                //->setCountryOfManufacture('AF') //country of manufacture (2-letter country code)
                                ->setPrice($step2['latest_base_price']) //price in form 11.22
                                ->setCost($step2['latest_base_price']) //price in form 11.22
                                // ->setSpecialPrice(00.44) //special price in form 11.22
                                // ->setSpecialFromDate('06/1/2015') //special price from (MM-DD-YYYY)
                                // ->setSpecialToDate('06/30/2015') //special price to (MM-DD-YYYY)
                                // ->setMsrpEnabled(1) //enable MAP
                                //  ->setMsrpDisplayActualPriceType(1) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
                                //  ->setMsrp(99.99) //Manufacturer's Suggested Retail Price
                                //  ->setMetaTitle('test meta title 2')
                                //  ->setMetaKeyword('test meta keyword 2')
                                //  ->setMetaDescription('test meta description 2')
                                ->setDescription($product_description)
                                ->setShortDescription($product_description)
                                // ->setMediaGallery(array('images' => array(), 'values' => array())) //media gallery initialization
                                // ->addImageToMediaGallery('media/catalog/product/1/0/10243-1.png', array('image','thumbnail','small_image'), false, false) 
                                ->setCategoryIds($category); //assign product to categories
                        $simpleProduct->save();
                        /*
                          $mediaGalleryBackendModel = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'media_gallery')->getBackend();
                          $arrayToMassAdd = array();

                          foreach($product_image as $key=>$val){

                          $importData=array("media_gallery"=>"/".$key,"image"=>"/".$key,
                          "small_image"=>"/".$key,"thumbnail"=>"/".$key

                          );
                          foreach ($simpleProduct->getMediaAttributes() as $mediaAttributeCode => $mediaAttribute) {
                          if (isset($importData[$mediaAttributeCode])) {
                          $file = trim($importData[$mediaAttributeCode]);
                          if (!empty($file) && !$mediaGalleryBackendModel->getImage($simpleProduct, $file)) {
                          $arrayToMassAdd[] = array('file' => trim($file), 'mediaAttribute' => $mediaAttributeCode);
                          }
                          }

                          }
                          }


                          $addedFilesCorrespondence = $mediaGalleryBackendModel->addImagesWithDifferentMediaAttributes(
                          $simpleProduct,
                          $arrayToMassAdd, Mage::getBaseDir('media') . DS . 'import',
                          true,
                          false
                          );
                         */

                        // $simpleProduct->save();



                        $stockData['use_config_manage_stock'] = 1;
                        $stockData['manage_stock'] = 1;
                        $stockData['use_config_min_qty'] = 1;
                        $stockData['max_sale_qty'] = 1;
                        $stockData['is_in_stock'] = 1;
                        $stockData['qty'] = $step2['sales_target'];




                        $productId = $simpleProduct->getId();
                        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
                        $stockItemId = $stockItem->getId();
                        $stock = array();

                        if (!$stockItemId) {
                            $stockItem->setData('product_id', $productId);
                            $stockItem->setData('stock_id', 1);
                        } else {
                            $stock = $stockItem->getData();
                        }


                        foreach ($stockData as $field => $value) {

                            $stockItem->setData($field, $value ? $value : 0);
                        }
                        $stockItem->setTypeId($simpleProduct->getTypeId());


                        $stockItem->save();

                        $simplepro[] = array("pid" => $productId, "colorattr" => $attr_id, "label" => $selcolor, "color_index" => $color_id, "sizeattr" => $size_attr_id, "size_label" => $size_val, "size_index" => $size_id);
                    } catch (Exception $e) {
                        Mage::log($e->getMessage());
                        echo $e->getMessage();
                    }
                }
            }
        }
        
        }

}

//configurable product 

        $configProduct = Mage::getModel('catalog/product');
        $sku = strtolower($customerData['firstname']) . "_" . uniqid();
        try {
            $configProduct
                    //    ->setStoreId(1) //you can set data in store scope
                    ->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
                    ->setAttributeSetId(4) //ID of a attribute set named 'default'
                    ->setTypeId('configurable') //product type
                    ->setCreatedAt(strtotime('now')) //product creation time
                    ->setCampaignDuration($step2['campaign_duration'])
                    ->setProfitPrice($step2['latest_profit_price'])
                    ->setUrlKey($step3['campaign_url'])
                    ->setSalesTarget($step2['sales_target'])
                    ->setCampaignStatus($running_option_id)
                    // ->setColorDescription($step2['color_description'])
                    ->setUserId($customer->getId())
                    ->setSku($sku) //SKU
                    ->setName($step1['campaign_name']) //product name
                    ->setWeight(0)
                    ->setStatus(2) //product status (1 - enabled, 2 - disabled)
                    ->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
                    ->setPrice($step2['latest_base_price']) //price in form 11.22
                    ->setCost($step2['latest_base_price']) //price in form 11.22
                    ->setDescription($product_description)
                    ->setShortDescription($product_description)
                    ->setCategoryIds($category); //assign product to categories
            $configProduct->save();

            $mediaGalleryBackendModel = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'media_gallery')->getBackend();
            $arrayToMassAdd = array();

            foreach ($product_image as $key => $val) {

                $importData = array("media_gallery" => "/" . $key, "image" => "/" . $key,
                    "small_image" => "/" . $key, "thumbnail" => "/" . $key
                );
                $path = Mage::getBaseDir('media') . DS . 'import' . DS.$key;
                if(file_exists($path)){
                foreach ($configProduct->getMediaAttributes() as $mediaAttributeCode => $mediaAttribute) {
                    if (isset($importData[$mediaAttributeCode])) {
                        $file = trim($importData[$mediaAttributeCode]);
                        if (!empty($file) && !$mediaGalleryBackendModel->getImage($configProduct, $file)) {
                            $arrayToMassAdd[] = array('file' => trim($file), 'mediaAttribute' => $mediaAttributeCode);
                        }
                    }
                }
                
                $addedFilesCorrespondence = $mediaGalleryBackendModel->addImagesWithDifferentMediaAttributes(
                    $configProduct, $arrayToMassAdd, Mage::getBaseDir('media') . DS . 'import', false, false);
                }
                
                 
            }


           


            /**/
            /** assigning associated product to configurable */
            /**/
            $configProduct->getTypeInstance()->setUsedProductAttributeIds(array($attr_id, $size_attr_id)); //attribute ID of attribute 'color' in my store
            $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();

            $configProduct->setCanSaveConfigurableAttributes(true);
            $configProduct->setConfigurableAttributesData($configurableAttributesData);

            $configurableProductsData = array();


            foreach ($simplepro as $spid) {



                $configurableProductsData[$spid['pid']] = array(//['920'] = id of a simple product associated with this configurable
                    '0' => array(
                        '0' => array(
                            'label' => $spid['label'], //attribute label
                            'attribute_id' => $spid['colorattr'], //attribute ID of attribute 'color' in my store
                            'value_index' => $spid['color_index'], //value of 'Green' index of the attribute 'color'
                            'is_percent' => '0', //fixed/percent price for this option
                            'pricing_value' => '' //value for the pricing
                        ),
                        '1' => array(
                            'label' => $spid['size_label'], //attribute label
                            'attribute_id' => $spid['sizeattr'], //attribute ID of attribute 'color' in my store
                            'value_index' => $spid['size_index'], //value of 'Green' index of the attribute 'color'
                            'is_percent' => '0', //fixed/percent price for this option
                            'pricing_value' => '' //value for the pricing
                        )
                    ),
                );
            }

            // print '<pre>';
            // print_r($configurableProductsData);


            $configProduct->setConfigurableProductsData($configurableProductsData);


            $configProduct->save();



            $stockData['use_config_manage_stock'] = 1;
            $stockData['manage_stock'] = 1;
            $stockData['use_config_min_qty'] = 1;
            $stockData['max_sale_qty'] = 1;
            $stockData['is_in_stock'] = 1;
            $stockData['qty'] = $step2['sales_target'];




            $productId = $configProduct->getId();
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
            $stockItemId = $stockItem->getId();
            $stock = array();

            if (!$stockItemId) {
                $stockItem->setData('product_id', $productId);
                $stockItem->setData('stock_id', 1);
            } else {
                $stock = $stockItem->getData();
            }


            foreach ($stockData as $field => $value) {

                $stockItem->setData($field, $value ? $value : 0);
            }
            $stockItem->setTypeId($configProduct->getTypeId());


            $stockItem->save();

            //custom cod to add special feature

            $specail_feature = $step2['special_features'];
            if (is_array($specail_feature) && count($specail_feature) > 0) {
                foreach ($specail_feature as $key => $val) {

                    $special['product_id'] = $productId;
                    $special['feature'] = $val;

                    $model = Mage::getModel('specialfeature/specialfeature')->setData($special);
                    $model->save();
                }
            }
            // end
            //custom code to add raw design
            if (is_array($raw_design) && count($raw_design) > 0) {
                foreach ($raw_design as $key => $val) {

                    $decode = json_decode($val);

                    $raw['product_id'] = $productId;
                    $raw['path'] = $decode->path;
                    $raw['name'] = $decode->name;

                    $model = Mage::getModel('rawdesign/rawdesign')->setData($raw);
                    $model->save();
                }
            }
            //end
            //custom code to add product featrures
            $product_features = $step2['product_features'];
            $product_description = $step2['product_description'];
            
            
            
            
            if (is_array($product_features) && count($product_features) > 0) {
                foreach ($product_features as $key => $val) {
                    if ($val != "") {
                        $exp = explode("_", $val);
                        $feature_id = $exp[1];

                        $fea['feature_id'] = $feature_id;
                        $fea['product_id'] = $productId;
                        
                        
                        $fea['description'] = $product_description[$exp[2]];


                        $model = Mage::getModel('featuredescription/featuredescription')->setData($fea);
                        $model->save();
                    }
                }
            }
            //end
            //custom code to add user for a product

            $user['product_id'] = $productId;
            $user['customer_id'] = $customer->getId();

            //  $model = Mage::getModel('userproduct/userproduct')->setData($user);
            //   $model->save();
            //end
            //custom code to add color description
            if (is_array($step2['color_value']) && count($step2['color_value']) > 0) {
                foreach ($step2['color_value'] as $key => $value) {
                    $des['product_id'] = $productId;
                    $des['code'] = $value;
                    if ($step2['color_description'][$key] != "") {
                        $des['description'] = $step2['color_description'][$key];
                    } else {
                        $des['description'] = "";
                    }

                    $model = Mage::getModel('colordescription/colordescription')->setData($des);
                    $model->save();
                }
            }
            //end
            
            //load the custom template to the email
            
            $emailTemplate = Mage::getModel('core/email_template')->loadDefault('custom_email_template1');
            
            $storename=Mage::getStoreConfig('trans_email/ident_general/name'); /* Receiver Email */
	    
	    $storeemail=Mage::getStoreConfig('trans_email/ident_general/email'); /* Receiver Name */
            
            $emailTemplate->setSenderName($storename);  /* Sender Name */
            $emailTemplate->setSenderEmail($storeemail); /* Sender Email */
            
            $templateParams=array("product"=>$configProduct,'sku'=>$sku);
            
            $emailTemplate->send($storeemail, $storename, $templateParams);
            

            //end email tempalte


            Mage::getSingleton('core/session')->addSuccess('Campaign Created Successfully');
            Mage::getSingleton('core/session')->unsStepOne();
            Mage::getSingleton('core/session')->unsStepTwo();
            Mage::getSingleton('core/session')->unsRawDesign();
            Mage::getSingleton('core/session')->unsProductImage();
            Mage::getSingleton('core/session')->unsStepThree();
            $this->_redirect('campaign/create/stepone');
        } catch (Exception $e) {
            Mage::log($e->getMessage());
            echo $e->getMessage();
        }
    }

    
}
