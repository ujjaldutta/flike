-- add table prefix if you have one
DELETE FROM eav_attribute WHERE entity_type_id IN (SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code = 'uipl_slider_slider');
DELETE FROM eav_entity_type WHERE entity_type_code = 'uipl_slider_slider';
DROP TABLE IF EXISTS uipl_slider_slider_int;
DROP TABLE IF EXISTS uipl_slider_slider_decimal;
DROP TABLE IF EXISTS uipl_slider_slider_datetime;
DROP TABLE IF EXISTS uipl_slider_slider_varchar;
DROP TABLE IF EXISTS uipl_slider_slider_text;
DROP TABLE IF EXISTS uipl_slider_slider;
DROP TABLE IF EXISTS uipl_slider_eav_attribute;
DELETE FROM core_resource WHERE code = 'uipl_slider_setup';
DELETE FROM core_config_data WHERE path like 'uipl_slider/%';